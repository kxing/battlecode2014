package team213.bots;

import battlecode.common.*;
import team213.debugging.*;
import team213.utils.*;

/**
 *
 * @author kxing
 */
public class NoisetowerBot extends Bot {
  private final MapLocation MY_LOCATION;
  private final int MAP_WIDTH;
  private final int MAP_HEIGHT;
  
  private MapLocation idealPastrLocation;

  // Key points to rally in cows.
  private MapLocation[] targetLocations;
  private int targetIndex;

  private MapLocation currentCowsLocation;

  private Direction[][] directionToPastr;
  private double[][] cowGrowths;

  private static final int MAXIMUM_CONTROLLABLE_DISTANCE =
          (int)(Math.sqrt(RobotType.NOISETOWER.attackRadiusMaxSquared) - 1) *
          (int)(Math.sqrt(RobotType.NOISETOWER.attackRadiusMaxSquared) - 1);

  public NoisetowerBot(RobotController rc) {
    super(rc);
    this.MY_LOCATION = rc.getLocation();
    this.MAP_WIDTH = rc.getMapWidth();
    this.MAP_HEIGHT = rc.getMapHeight();

    this.targetIndex = 0;

    this.directionToPastr = new Direction[MAP_WIDTH][MAP_HEIGHT];
    this.cowGrowths = rc.senseCowGrowth();
    broadcastAsImmobile();
  }

  public void performRoundActions() throws GameActionException {
    if (idealPastrLocation == null) {
      idealPastrLocation = communication.receivePastrLocation();
      computeTargetLocations();
      initializeForHerding();
    }

    if (rc.isActive()) {
      herdCows();
    }
//    rc.setIndicatorString(0, currentCowsLocation.toString() + " " + targetIndex);

    // Set the defender counts.
    int enemyCount = rc.senseNearbyGameObjects(Robot.class, SENSOR_RADIUS_SQUARED, ENEMY_TEAM).length;
    int defendersNecessary = enemyCount + DEFAULT_PASTR_DEFENDERS;
    if (communication.receivePastrDefendersNecessary() != defendersNecessary) {
      communication.sendPastrDefendersNecessary(defendersNecessary);
    }
    
    return;
  }

  private void initializeForHerding() {
    this.targetIndex = 0;
    this.currentCowsLocation = targetLocations[targetIndex];
  }

  private boolean herdCows() throws GameActionException {
    Assert.debug_Assert(targetLocations != null);

    if (currentCowsLocation.equals(idealPastrLocation)) {
//    if (currentCowsLocation.distanceSquaredTo(idealPastrLocation) <= GameConstants.PASTR_RANGE) {
      // Reset for the next clump of cows.
      targetIndex = (targetIndex + 1) % targetLocations.length;
      currentCowsLocation = targetLocations[targetIndex];
    }

    Direction directionToMove = currentCowsLocation.directionTo(idealPastrLocation);
    if (finishedComputation) {
      Direction betterDirection = directionToPastr[currentCowsLocation.x][currentCowsLocation.y];
      if (betterDirection != null) {
        // The chosen direction should be more efficient.
        directionToMove = betterDirection;
      }
    }

    boolean success = moveCows(currentCowsLocation, directionToMove);

    currentCowsLocation = currentCowsLocation.add(directionToMove);

    return success;
  }

  // Shoots so that cows at the given location move in the given direction.
  private boolean moveCows(MapLocation ml, Direction d) throws GameActionException {
    Direction oppositeDirection = d.opposite();
    MapLocation shootingLocation = ml.add(oppositeDirection);
    if (shootingLocation.distanceSquaredTo(MY_LOCATION) > ATTACK_RADIUS_SQUARED) {
      return false;
    }
    MapLocation candidateSquare = shootingLocation.add(oppositeDirection);
    while (candidateSquare.distanceSquaredTo(MY_LOCATION) <= ATTACK_RADIUS_SQUARED &&
           candidateSquare.distanceSquaredTo(ml) <= GameConstants.ATTACK_SCARE_RANGE) {
      shootingLocation = candidateSquare;
      candidateSquare = shootingLocation.add(oppositeDirection);
    }

    rc.attackSquare(shootingLocation);
    return true;
  }

  private static final double EPSILON = 0.1;

  // Computes shooting squares really quickly, but is probably very suboptimal.
  private void computeTargetLocations() {
    MapLocation[] tempTargetLocations = new MapLocation[MAP_WIDTH * MAP_HEIGHT];
    int tempTargetLocationsUsed = 0;

    // Really derpy shooting code.
    for (Direction d : Utils.MOVEMENT_DIRECTIONS) {
      // Go out as far as you can.
      MapLocation target = idealPastrLocation;
      MapLocation shootingTarget = idealPastrLocation;
      while (shootingTarget.add(d).distanceSquaredTo(idealPastrLocation) <= GameConstants.ATTACK_SCARE_RANGE) {
        shootingTarget = shootingTarget.add(d);
      }

      MapLocation bestTarget = target;

      while (shootingTarget.add(d).distanceSquaredTo(MY_LOCATION) <= ATTACK_RADIUS_SQUARED &&
             rc.senseTerrainTile(target.add(d)) != TerrainTile.OFF_MAP) {
        target = target.add(d);
        shootingTarget = shootingTarget.add(d);

        if (!finishedComputation || (directionToPastr[target.x][target.y] != null && cowGrowths[target.x][target.y] > EPSILON)) {
          bestTarget = target;
        }
      }

      if (!bestTarget.equals(idealPastrLocation)) {
        tempTargetLocations[tempTargetLocationsUsed++] = bestTarget;
      }
    }

    targetLocations = new MapLocation[tempTargetLocationsUsed];
    System.arraycopy(tempTargetLocations, 0, targetLocations, 0, tempTargetLocationsUsed);
  }

  private static final int PIVOT_LENGTH = 2;

  private void pivot() throws GameActionException {
//    if (!rc.canSenseSquare(currentCowsLocation)) {
//      return;
//    }
//    double currentBestCows = rc.senseCowsAtLocation(currentCowsLocation);
//    MapLocation currentBestLocation = currentCowsLocation;
//    
//    for (int dx = -PIVOT_LENGTH; dx <= PIVOT_LENGTH; dx++) {
//      for (int dy = -PIVOT_LENGTH; dy <= PIVOT_LENGTH; dy++) {
//        MapLocation testLocation = new MapLocation(currentCowsLocation.x + dx, currentCowsLocation.y + dy);
//        if (!rc.canSenseSquare(testLocation)) {
//          continue;
//        }
//        if (testLocation.distanceSquaredTo(idealPastrLocation) <= GameConstants.PASTR_RANGE) {
//          continue;
//        }
//        double amount = rc.senseCowsAtLocation(testLocation);
//        if (currentBestCows < amount) {
//          currentBestCows = amount;
//          currentBestLocation = testLocation;
//        }
//      }
//    }
//    currentCowsLocation = currentBestLocation;
  }

  private class ComputationState {
    private boolean[][] queued;
    private MapLocation[] queue;
    private int queueStart;
    private int queueEnd;

    public ComputationState() {
      // Auto-initialize array to false'es.
      this.queued = new boolean[MAP_WIDTH][MAP_HEIGHT];
      this.queue = new MapLocation[MAP_WIDTH * MAP_HEIGHT];
      this.queueStart = 0;
      this.queueEnd = 0;
    }

    public boolean isQueueEmpty() {
      return queueStart >= queueEnd;
    }

    public MapLocation removeFromQueue() {
      return queue[queueStart++];
    }

    public void addToQueue(MapLocation ml) {
      queue[queueEnd++] = ml;
      queued[ml.x][ml.y] = true;
    }

    public boolean isInQueue(MapLocation ml) {
      return queued[ml.x][ml.y];
    }
  }

  private ComputationState computationState = null;
  private boolean finishedComputation = false;

  // Computes the optimal directions to corral the cows.
  public boolean performLongComputation() throws GameActionException {
    if (idealPastrLocation == null) {
      // We can't really do the computation without knowing where the pastr is.
      return false;
    }
    if (finishedComputation) {
      // We've already finished the computation.
      return false;
    }

    if (computationState == null) {
      computationState = new ComputationState();

      computationState.addToQueue(idealPastrLocation);
      directionToPastr[idealPastrLocation.x][idealPastrLocation.y] = Direction.NONE;
      return true;
    }

    if (!computationState.isQueueEmpty()) {
      MapLocation currentLocation = computationState.removeFromQueue();
      for (Direction d : Utils.MOVEMENT_DIRECTIONS) {
        MapLocation candidateLocation = currentLocation.add(d);
        if (candidateLocation.distanceSquaredTo(MY_LOCATION) >= MAXIMUM_CONTROLLABLE_DISTANCE) {
          // Too far away.
          continue;
        }
        if (!Utils.isTraversable(rc, candidateLocation)) {
          continue;
        }
        if (computationState.isInQueue(candidateLocation)) {
          continue;
        }
        computationState.addToQueue(candidateLocation);
        directionToPastr[candidateLocation.x][candidateLocation.y] = d.opposite();
      }
      return true;
    }

    finishedComputation = true;
    computeTargetLocations();
    
    return false;
  }

}
