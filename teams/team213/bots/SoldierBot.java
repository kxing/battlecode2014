package team213.bots;

import battlecode.common.*;
import team213.communication.*;
import team213.debugging.*;
import team213.nav.*;
import team213.sensing.*;
import team213.utils.*;

/**
 * Code for soldier-specific behavior.
 *
 * @author kxing
 */
public class SoldierBot extends Bot {
  private BugNav bugNav;

  private MapLocation MY_LOCATION;

  private static final double NEAR_ENEMY_PASTR_BONUS = 5.0;
  private static final double ENEMY_SOLDIER_RANGE_PENALTY = -10.0;
  private static final double ENEMY_SOLDIER_SURROUND_BONUS = 5.0;
  private static final double ENEMY_TOWER_RANGE_PENALTY = -50.0;
  private static final double ENEMY_TOWER_SPLASH_PENALTY = -25.0;
  private static final double ILLEGAL_MOVE_PENALTY = -1.0e9;

  // Happens to be the suicide base damage.
  private static final double LOW_HEALTH = 40.0;
  private static final double LOTS_OF_HEALTH = 90.0;

  private enum SoldierType {
    RUSHER,
    PASTR_BUILDER_OR_DEFENDER,
    NOISETOWER_BUILDER,
  };

  private SoldierType type;

  private boolean isTeamFarming;
  private MapLocation idealPastrLocation;
  private MapLocation idealNoisetowerLocation;

  private Objective currentObjective;
  private MapLocation currentGoal;
  private DirectionEvaluations directionEvaluations;
  private SensorInfo sensorInfo;

  private boolean intendToSelfDestruct;

  private static final double ACTION_DELAY_UNSAFE = 0.5;
  private static final double ACTION_DEALY_REDUCTION_BONUS = 0.5;

  private static final double ENEMY_COWS_THRESHOLD = 50.0;

  public static final int FARMING_QUOTA = 8;

  private static final int ENEMY_PASTR_CLOSE_DISTANCE = 41;
  private static final int ENEMY_TARGET_CLOSE_DISTANCE = 34;

  private static final int TWO_SQUARES_AWAY = 8;

  public SoldierBot(RobotController rc) {
    super(rc);
    this.bugNav = new BugNav(rc, communication);

    this.currentObjective = null;
    this.currentGoal = rc.senseEnemyHQLocation();
    this.directionEvaluations = new DirectionEvaluations();
    this.bugNav.setGoal(currentGoal);
    this.sensorInfo = new SensorInfo(rc);

    this.type = SoldierType.RUSHER;
    this.intendToSelfDestruct = false;

    this.isTeamFarming = false;
  }

  public void performRoundActions() throws GameActionException {
    MY_LOCATION = rc.getLocation();

    if (intendToSelfDestruct) {
      considerSelfDestruct();
    }

    if (!rc.isActive()) {
      considerSelfDestruct();
      return;
    }
    intendToSelfDestruct = false;
    
    sensorInfo.clear();
    directionEvaluations.clear();

    dissuadeFromEnemyHeadquarters();

    // Consider making a move to self-destruct.
    if (considerSelfDestructMove()) {
      bugNav.setGoal(currentGoal);
      return;
    }

    // Stop if we have made an attacking move.
    if (attackingMove()) {
      bugNav.setGoal(currentGoal);
      return;
    }
    
    handleBroadcasts();
    adjustSoldierType();
//    rc.setIndicatorString(0, type.toString());

    MapLocation newGoal = getGoal();
    if (!currentGoal.equals(newGoal)) {
      currentGoal = newGoal;
      bugNav.setGoal(newGoal);
    }

    switch (type) {
      case RUSHER:
        if (runRusherCode()) {
          return;
        }
        break;
      case PASTR_BUILDER_OR_DEFENDER:
        if (runPastrBuilderOrDefenderCode()) {
          return;
        }
        break;
      case NOISETOWER_BUILDER:
        if (runNoisetowerBuilderCode()) {
          return;
        }
        break;
      default:
        Assert.debug_Assert(false);
    }

    bugNav.moveTowardsGoal(directionEvaluations, shouldSneak());
  }

  private static final int ENEMY_HEADQUARTERS_CLOSE = 52;

  private void dissuadeFromEnemyHeadquarters() throws GameActionException {
    if (MY_LOCATION.distanceSquaredTo(ENEMY_TEAM_HEADQUARTERS) > ENEMY_HEADQUARTERS_CLOSE) {
      return;
    }

    for (int i = 0; i < DirectionEvaluations.DIRECTIONS.length; i++) {
      Direction d = DirectionEvaluations.DIRECTIONS[i];
      MapLocation candidateSquare = MY_LOCATION.add(d);

      if (ENEMY_TEAM_HEADQUARTERS.distanceSquaredTo(candidateSquare) <= RobotType.HQ.attackRadiusMaxSquared) {
        // Don't stand within headquarters range.
        directionEvaluations.addToDirection(
                ENEMY_TOWER_RANGE_PENALTY + ENEMY_TOWER_RANGE_PENALTY /
                (ENEMY_TEAM_HEADQUARTERS.distanceSquaredTo(candidateSquare) + 1), i);
      } else {
        Direction splashDirection = ENEMY_TEAM_HEADQUARTERS.directionTo(candidateSquare);
        if (ENEMY_TEAM_HEADQUARTERS.add(splashDirection).distanceSquaredTo(candidateSquare) <= RobotType.HQ.attackRadiusMaxSquared) {
          // Don't stand within splash range.
          directionEvaluations.addToDirection(ENEMY_TOWER_SPLASH_PENALTY, i);
        }
      }
    }
  }

  private MapLocation getGoal() throws GameActionException {
    if (type == SoldierType.PASTR_BUILDER_OR_DEFENDER && idealPastrLocation != null) {
      return idealPastrLocation;
    }

    if (type == SoldierType.NOISETOWER_BUILDER && idealNoisetowerLocation != null) {
      return idealNoisetowerLocation;
    }

    // The default shouldn't really matter, because the headquarters should
    // always be calling the shots.
    MapLocation bestTarget = ENEMY_TEAM_HEADQUARTERS;
    currentObjective = communication.receiveObjective();
    if (currentObjective != null) {
      bestTarget = currentObjective.target;
    }

    return bestTarget;
  }

  /**
   * Returns whether there is a clear path between two locations (ie. not
   * separated by voids).
   *
   * Note this method is pretty expensive, so don't call this multiple times!
   *
   * @param start The starting location.
   * @param target The target location.
   * @return Whether the locations are not separated by voids.
   */
  private boolean unhinderedPathTo(MapLocation start, MapLocation target) throws GameActionException {
    MapLocation current = start;
    while (!current.equals(target)) {
      Direction d = current.directionTo(target);
      current = current.add(d);
      if (!Utils.isTraversable(rc, current) || rc.senseObjectAtLocation(current) != null) {
        return false;
      }
    }
    return true;
  }

  // Returns if we should refrain from taking any other actions
  // (ie. moving or attacking).
  private boolean attackingMove() throws GameActionException {
    Robot[] inRangeEnemies = rc.senseNearbyGameObjects(Robot.class, ATTACK_RADIUS_SQUARED, ENEMY_TEAM);
    if (inRangeEnemies.length > 0) {
      makeInRangeMove(inRangeEnemies);
      return true;
    }
    
    return makeNotInRangeMove();
  }

  private static final int ADJACENT = 2;
  private static final double ENEMY_SELF_DESTRUCT_BUFFER = 30.0;

  /**
   * Called when the bot is within attack range of another robot.
   */
  private void makeInRangeMove(Robot[] inRangeEnemies) throws GameActionException {
    Robot[] adjacentEnemies = rc.senseNearbyGameObjects(Robot.class, ADJACENT, ENEMY_TEAM);

    // Run away if an enemy soldier is trying to self-destruct.
    for (Robot r : adjacentEnemies) {
      RobotInfo ri = rc.senseRobotInfo(r);
      if (ri.type == RobotType.SOLDIER) {
        double splashDamage = GameConstants.SELF_DESTRUCT_BASE_DAMAGE +
                              GameConstants.SELF_DESTRUCT_DAMAGE_FACTOR * ri.health;
        if (splashDamage + ENEMY_SELF_DESTRUCT_BUFFER > rc.getHealth()) {
          if (runAway(ri.location)) {
            addSpottedEnemy(ri.location);
            return;
          }
        }
      }
    }

    RobotInfo[] inRangeInfos = new RobotInfo[inRangeEnemies.length];
    MapLocation[] enemyLocations = new MapLocation[inRangeEnemies.length];
    int numberOfEnemySoldiers = 0;
    int numberOfDangerousEnemies = 0;
    double maxDamageTaken = 0.0;

    boolean canKill = false;

    MapLocation spottedEnemy = null;

    for (int i = 0; i < inRangeEnemies.length; i++) {
      RobotInfo ri = rc.senseRobotInfo(inRangeEnemies[i]);
      inRangeInfos[i] = ri;

      if (ri.type != RobotType.SOLDIER || ri.actionDelay > RobotType.SOLDIER.attackDelay + 1.0) {
        continue;
      }

      spottedEnemy = ri.location;
      
      maxDamageTaken += RobotType.SOLDIER.attackPower;
      enemyLocations[numberOfEnemySoldiers++] = ri.location;

      if (rc.senseNearbyGameObjects(Robot.class, ri.location, ATTACK_RADIUS_SQUARED, MY_TEAM).length == 0) {
        numberOfDangerousEnemies++;
      }

      if (ri.health <= RobotType.SOLDIER.attackPower) {
        canKill = true;
      }
    }

    if (spottedEnemy != null) {
      addSpottedEnemy(spottedEnemy);
    }

    if (!canKill && (maxDamageTaken >= rc.getHealth() || numberOfDangerousEnemies > 1)) {
      MapLocation enemyCentroid = Utils.getCentroid(enemyLocations, numberOfEnemySoldiers);
      if (MY_LOCATION.distanceSquaredTo(enemyCentroid) <= ADJACENT) {
        // We're surrounded - we're screwed, so we'll try to fire off as many shots as possible.
        attack(inRangeInfos);
        return;
      }
      if (runAway(enemyCentroid)) {
        // We're running away.
        return;
      }
      // Whatever - we can't run, so we'll try to get in as much damage as possible.
      attack(inRangeInfos);
      return;
    }

    // This should be a reasonable engagement - we'll attack.
    attack(inRangeInfos);
    return;
  }

  private void attack(RobotInfo[] inRangeInfos) throws GameActionException {
    RobotInfo bestEnemyTarget = null;

    for (RobotInfo ri : inRangeInfos) {
      if (isBetterTarget(ri, bestEnemyTarget)) {
        bestEnemyTarget = ri;
      }
    }

    if (bestEnemyTarget != null) {
      attackSquare(bestEnemyTarget.location);
    }
  }

  private boolean isBetterTarget(RobotInfo candidate, RobotInfo currentBest) {
    if (currentBest == null) {
      return true;
    }
    if (candidate.type == currentBest.type && candidate.type != RobotType.SOLDIER) {
      // Prioritize enemy pastrs and noisetowers by looking at HP.
      return candidate.health < currentBest.health;
    }
    switch (candidate.type) {
      case SOLDIER:
        if (currentBest.type != RobotType.SOLDIER) {
          return true;
        }
        break;
      case PASTR:
        return (currentBest.type == RobotType.NOISETOWER);
      case NOISETOWER:
        return false;
      default:
        Assert.debug_Assert(false);
    }

    double candidateHealth = candidate.health;
    if (MY_LOCATION.distanceSquaredTo(candidate.location) <= TWO_SQUARES_AWAY) {
      candidateHealth /= 2.0;
    }
    double currentBestHealth = currentBest.health;
    if (MY_LOCATION.distanceSquaredTo(currentBest.location) <= TWO_SQUARES_AWAY) {
      currentBestHealth /= 2.0;
    }

    return (candidateHealth < currentBestHealth);
  }

  private static final int NEARLY_IN_RANGE = 20;
  private static final int ENEMY_SURROUND_LIMIT = 3;

  private boolean makeNotInRangeMove() throws GameActionException {
    // For right now, just don't engage.
    Robot[] enemyRobots = rc.senseNearbyGameObjects(Robot.class, NEARLY_IN_RANGE, ENEMY_TEAM);
    RobotInfo[] enemyInfos = new RobotInfo[enemyRobots.length];

    for (int i = 0; i < enemyRobots.length; i++) {
      enemyInfos[i] = rc.senseRobotInfo(enemyRobots[i]);
    }

    if (enemyInfos.length > 0) {
      addSpottedEnemy(enemyInfos[0].location);
    }

    for (RobotInfo ri : enemyInfos) {
      Direction directionToEnemy = MY_LOCATION.directionTo(ri.location);
      Direction[] directionsNearEnemy = new Direction[] {
        directionToEnemy,
        directionToEnemy.rotateLeft(),
        directionToEnemy.rotateRight(),
      };
      switch (ri.type) {
        case SOLDIER:
          boolean shouldAttack = false;
          if (rc.getHealth() >= LOW_HEALTH) {
            if (rc.senseNearbyGameObjects(Robot.class, ri.location, ATTACK_RADIUS_SQUARED, MY_TEAM).length > 0) {
              // Some ally is in range.
              shouldAttack = true;
            } else if (enemyInfos.length <= ENEMY_SURROUND_LIMIT && ri.location.distanceSquaredTo(ENEMY_TEAM_HEADQUARTERS) > ENEMY_HEADQUARTERS_CLOSE) {
              // We should be able to outnumber the enemy.
              int nearbyFriends = rc.senseNearbyGameObjects(Robot.class, ri.location, NEARLY_IN_RANGE, MY_TEAM).length;
              int nearbyEnemies = rc.senseNearbyGameObjects(Robot.class, ri.location, NEARLY_IN_RANGE, ENEMY_TEAM).length;
              if (nearbyFriends >= nearbyEnemies) {
                shouldAttack = true;
              }
            }
          }

          if (shouldAttack) {
            for (Direction d : directionsNearEnemy) {
              if (MY_LOCATION.add(d).distanceSquaredTo(ri.location) <= RobotType.SOLDIER.attackRadiusMaxSquared) {
                directionEvaluations.addToDirection(ENEMY_SOLDIER_SURROUND_BONUS, d);
              }
            }
          } else {
            // Don't engage if we can't win.
            for (Direction d : directionsNearEnemy) {
              if (MY_LOCATION.add(d).distanceSquaredTo(ri.location) <= RobotType.SOLDIER.attackRadiusMaxSquared) {
                directionEvaluations.addToDirection(ENEMY_SOLDIER_RANGE_PENALTY, d);
              }
            }
          }
          break;
        case PASTR:
        case NOISETOWER:
          if (unhinderedPathTo(MY_LOCATION, ri.location) && type == SoldierType.RUSHER) {
            // Get closer to a pastr or noisetower, if we can go there directly.
            for (Direction d : directionsNearEnemy) {
              directionEvaluations.addToDirection(
                      NEAR_ENEMY_PASTR_BONUS / (MY_LOCATION.add(d).distanceSquaredTo(ri.location) + 1), d);
            }
          }
          break;
        default:
          break;
      }
    }

    for (Direction d : Utils.MOVEMENT_DIRECTIONS) {
      if (!rc.canMove(d)) {
        directionEvaluations.addToDirection(ILLEGAL_MOVE_PENALTY, d);
      }
    }

    if (directionEvaluations.isMoveForced()) {
      Direction d = directionEvaluations.getBestDirection();
      if (d != DirectionEvaluations.ATTACK_SENTINEL) {
        move(d);
        return true;
      }
    }

    return false;
  }

  private void attackEnemyCows() throws GameActionException {
    MapLocation bestTarget = null;
    Direction d = currentGoal.directionTo(MY_LOCATION);
    MapLocation[] potentialSquares = new MapLocation[] {
      currentGoal.add(d),
      currentGoal.add(d).add(d),
      currentGoal.add(d).add(d.rotateLeft()),
      currentGoal.add(d).add(d.rotateRight()),
      currentGoal.add(d).add(d).add(d),
      currentGoal.add(d).add(d).add(d.rotateLeft()),
      currentGoal.add(d).add(d).add(d.rotateRight()),
    };

    double mostCows = ENEMY_COWS_THRESHOLD;
    for (MapLocation ml : potentialSquares) {
      if (!rc.canAttackSquare(ml) ||
          rc.senseObjectAtLocation(ml) != null) {
        // Out of range or something in the square.
        continue;
      }
      double cows = rc.senseCowsAtLocation(ml);
      if (mostCows < cows) {
        bestTarget = ml;
        mostCows = cows;
      }
    }

    if (bestTarget != null) {
      attackSquare(bestTarget);
      return;
    }
  }

  /**
   * Attempts to run away.
   *
   * @param location The location from which we are trying to run away.
   * @return Whether the robot was successful in trying to run away.
   */
  private boolean runAway(MapLocation location) throws GameActionException {
    Direction optimalDirection = location.directionTo(MY_LOCATION);
    Direction[] runningDirections = new Direction[] {
      optimalDirection,
      optimalDirection.rotateLeft(),
      optimalDirection.rotateRight(),
    };

    for (Direction d : runningDirections) {
      if (rc.canMove(d)) {
        move(d);
        return true;
      }
    }

    return false;
  }

  public boolean performLongComputation() throws GameActionException {
    return false;
  }

  private void handleBroadcasts() throws GameActionException {
    if (idealPastrLocation == null && communication.receiveComputationsDone()) {
      // Handle pastr building.
      idealPastrLocation = communication.receivePastrLocation();
    }

    if (idealNoisetowerLocation == null && communication.receiveComputationsDone()) {
      // Handle noisetower building.
      idealNoisetowerLocation = communication.receiveNoisetowerLocation();
    }

    if (!isTeamFarming && communication.receiveFarmSignal()) {
      isTeamFarming = true;
    }
  }

  private void considerSelfDestruct() throws GameActionException {
    double totalDamage = -rc.getHealth();
    double splashDamage = GameConstants.SELF_DESTRUCT_BASE_DAMAGE +
            GameConstants.SELF_DESTRUCT_DAMAGE_FACTOR * rc.getHealth();

    for (Direction d : Utils.MOVEMENT_DIRECTIONS) {
      GameObject go = rc.senseObjectAtLocation(MY_LOCATION.add(d));
      if (go == null || !(go instanceof Robot)) {
        continue;
      }

      RobotInfo ri = rc.senseRobotInfo((Robot)(go));
      double damage = Math.min(ri.health, splashDamage);
      if (ri.team == MY_TEAM) {
        totalDamage -= damage;
      } else if (ri.team == ENEMY_TEAM) {
        totalDamage += damage;
      }
    }

    if (totalDamage > 0.0) {
      rc.selfDestruct();
      rc.yield();
    }
  }

  private boolean considerSelfDestructMove() throws GameActionException {
    sensorInfo.senseEnemyRobots(true);

    if (sensorInfo.getEnemyRobots().length == 0) {
      return false;
    }

    sensorInfo.senseMyRobots(true);

    double[] totalDamage = new double[Utils.MOVEMENT_DIRECTIONS.length];

    for (RobotInfo ri : sensorInfo.getEnemyInfos()) {
      if (MY_LOCATION.distanceSquaredTo(ri.location) > TWO_SQUARES_AWAY) {
        continue;
      }
      for (int i = 0; i < Utils.MOVEMENT_DIRECTIONS.length; i++) {
        MapLocation newLocation = MY_LOCATION.add(Utils.MOVEMENT_DIRECTIONS[i]);
        if (newLocation.isAdjacentTo(ri.location)) {
          totalDamage[i] += Math.min(ri.health, GameConstants.SELF_DESTRUCT_BASE_DAMAGE);
        }
      }
    }

    for (RobotInfo ri : sensorInfo.getMyInfos()) {
      if (MY_LOCATION.distanceSquaredTo(ri.location) > TWO_SQUARES_AWAY) {
        continue;
      }
      for (int i = 0; i < Utils.MOVEMENT_DIRECTIONS.length; i++) {
        MapLocation newLocation = MY_LOCATION.add(Utils.MOVEMENT_DIRECTIONS[i]);
        if (newLocation.isAdjacentTo(ri.location)) {
          totalDamage[i] -= Math.min(ri.health, GameConstants.SELF_DESTRUCT_BASE_DAMAGE);
        }
      }
    }

    double bestDamageTaken = Double.MAX_VALUE;
    Direction bestDirection = null;

    for (int i = 0; i < Utils.MOVEMENT_DIRECTIONS.length; i++) {
      if (totalDamage[i] <= rc.getHealth()) {
        // Not worth self-destructing.
        continue;
      }

      Direction direction = Utils.MOVEMENT_DIRECTIONS[i];
      if (!rc.canMove(direction)) {
        continue;
      }

      if (directionEvaluations.isDangerous(direction)) {
        // Avoid taking headquarters damage.
        continue;
      }

      double damageTaken = 0.0;

      MapLocation newLocation = MY_LOCATION.add(direction);
      for (RobotInfo ri : sensorInfo.getEnemyInfos()) {
        if (ri.type != RobotType.SOLDIER) {
          // Pastrs and noisetowers can't attack us.
          continue;
        }
        if (ri.location.distanceSquaredTo(newLocation) > RobotType.SOLDIER.attackRadiusMaxSquared) {
          // Too far away to attack us.
          continue;
        }
        if (ri.actionDelay >= 2.0 || (ri.robot.getID() > ID && ri.actionDelay > 1.0)) {
          // Can't actually attack us.
          continue;
        }

        damageTaken += RobotType.SOLDIER.attackPower;
      }

      if (bestDamageTaken > damageTaken) {
        bestDamageTaken = damageTaken;
        bestDirection = direction;
      }
    }

    if (bestDirection != null && bestDamageTaken < rc.getHealth()) {
      move(bestDirection);
      intendToSelfDestruct = true;
      return true;
    }
    
    return false;
  }

  private boolean isNoisetowerBuilt() throws GameActionException {
    if (idealNoisetowerLocation == null) {
      return false;
    }
    if (!rc.canSenseSquare(idealNoisetowerLocation)) {
      return false;
    }
    
    GameObject go = rc.senseObjectAtLocation(idealNoisetowerLocation);
    if (go != null && go instanceof Robot) {
      RobotInfo ri = rc.senseRobotInfo((Robot)(go));
      if (ri.type == RobotType.NOISETOWER && ri.team == MY_TEAM) {
        return true;
      }
    }

    return false;
  }

  private int getPastrDefenderIndex() throws GameActionException {
    int pastrDefendersClaimed = communication.receivePastrDefendersClaimed();
    int index = -1;
    for (int i = 0; i < pastrDefendersClaimed; i++) {
      if (communication.receivePastrDefenderId(i) == ID) {
        index = i;
        break;
      }
    }
    Assert.debug_Assert(index != -1);
    return index;
  }

  private void adjustSoldierType() throws GameActionException {
    // Handling the pastr defenders.
    int pastrDefendersNecessary = communication.receivePastrDefendersNecessary();
    int pastrDefendersClaimed = communication.receivePastrDefendersClaimed();
    
    switch (type) {
      case RUSHER:
        // Rusher -> noisetower builder.
        if (isTeamFarming && communication.receiveNoisetowerBuilderId() == Communication.NOBODY &&
            rc.senseNearbyGameObjects(Robot.class, idealNoisetowerLocation, 0, MY_TEAM).length == 0) {
          communication.sendNoisetowerBuilderId(ID);
          type = SoldierType.NOISETOWER_BUILDER;
          return;
        }

        // Rusher -> pastr defender.
        if ((rc.getHealth() <= LOW_HEALTH &&
              ((currentObjective != null && currentObjective.type != ObjectiveType.DESTROY) ||
               !rc.canSenseSquare(currentGoal))) ||
            pastrDefendersClaimed < pastrDefendersNecessary) {
          // Rusher -> Pastr defender.
          communication.sendPastrDefenderId(pastrDefendersClaimed, ID);
          communication.sendPastrDefendersClaimed(pastrDefendersClaimed + 1);
          type = SoldierType.PASTR_BUILDER_OR_DEFENDER;
          return;
        }
        break;
      case PASTR_BUILDER_OR_DEFENDER:
        if (pastrDefendersClaimed > pastrDefendersNecessary &&
            (idealPastrLocation == null ||
             !isTeamFarming ||
             !rc.canSenseSquare(idealPastrLocation) ||
             rc.senseNearbyGameObjects(Robot.class, idealPastrLocation, SENSOR_RADIUS_SQUARED, MY_TEAM).length >= pastrDefendersNecessary + 2) &&
            rc.getHealth() >= LOTS_OF_HEALTH) {
          // Pastr defender -> Rusher.
          int index = getPastrDefenderIndex();
          int lastId = communication.receivePastrDefenderId(pastrDefendersClaimed - 1);

          communication.sendPastrDefenderId(index, lastId);
          communication.sendPastrDefendersClaimed(pastrDefendersClaimed - 1);
          type = SoldierType.RUSHER;
          return;
        }
        break;
      default:
        break;
    }
  }

  private boolean runRusherCode() throws GameActionException {
    if (rc.sensePastrLocations(ENEMY_TEAM).length == 0) {
      return false;
    }
    // Anti-headquarters turtle code.
    if (currentGoal.distanceSquaredTo(ENEMY_TEAM_HEADQUARTERS) <= GameConstants.PASTR_RANGE &&
        MY_LOCATION.distanceSquaredTo(ENEMY_TEAM_HEADQUARTERS) <= ENEMY_TARGET_CLOSE_DISTANCE &&
        !directionEvaluations.isDangerous(DirectionEvaluations.ATTACK_SENTINEL)) {
      // Consider attacking cows.
      attackEnemyCows();
      noMovement();
      return true;
    }

    return false;
  }

  private boolean runPastrBuilderOrDefenderCode() throws GameActionException {
    if (isTeamFarming && MY_LOCATION.equals(idealPastrLocation) &&
        isNoisetowerBuilt()) {
      sensorInfo.senseMyRobots(false);
      if (sensorInfo.getMyRobots().length >= communication.receivePastrDefendersNecessary() &&
          rc.senseNearbyGameObjects(Robot.class, SENSOR_RADIUS_SQUARED, MY_TEAM).length >
          rc.senseNearbyGameObjects(Robot.class, SENSOR_RADIUS_SQUARED, ENEMY_TEAM).length) {
        rc.construct(RobotType.PASTR);
        noMovement();
        return true;
      }
    }

    if (isTeamFarming && MY_LOCATION.isAdjacentTo(idealPastrLocation) &&
        !MY_LOCATION.equals(idealNoisetowerLocation) &&
        !directionEvaluations.isDangerous(DirectionEvaluations.ATTACK_SENTINEL)) {
      GameObject go = rc.senseObjectAtLocation(idealPastrLocation);
      if (go != null && go.getTeam() == MY_TEAM) {
        // Do nothing, a pastr will be built -> stay put.
        noMovement();
        return true;
      }
    }
    return false;
  }

  private boolean runNoisetowerBuilderCode() throws GameActionException {
    if (isTeamFarming && MY_LOCATION.equals(idealNoisetowerLocation) &&
        rc.senseNearbyGameObjects(Robot.class, SENSOR_RADIUS_SQUARED, MY_TEAM).length >
        rc.senseNearbyGameObjects(Robot.class, SENSOR_RADIUS_SQUARED, ENEMY_TEAM).length) {
      rc.construct(RobotType.NOISETOWER);
      noMovement();
      return true;
    }
    return false;
  }

  private void addSpottedEnemy(MapLocation location) throws GameActionException {
    int numberOfSpottedEnemies = communication.receiveNumberOfSpottedEnemies();
    communication.sendSpottedEnemy(location, numberOfSpottedEnemies);
    communication.sendNumberOfSpottedEnemies(numberOfSpottedEnemies + 1);
  }

  private static final int SNEAK_THRESHOLD = 144;

  private boolean shouldSneak() {
    return (isTeamFarming && MY_LOCATION.distanceSquaredTo(idealPastrLocation) <= SNEAK_THRESHOLD);
  }
}
