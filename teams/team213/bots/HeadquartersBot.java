package team213.bots;

import battlecode.common.*;
import team213.communication.*;
import team213.sensing.*;
import team213.utils.*;

/**
 * Code for headquarters-specific behavior.
 *
 * @author kxing
 */
public class HeadquartersBot extends Bot {

  private final MapLocation MY_LOCATION;
  private final double[][] COW_GROWTHS;
  private final int MAP_WIDTH;
  private final int MAP_HEIGHT;

  private final MapLocation[] spawnSquares;
  private final SensorInfo sensorInfo;

  private boolean computedFarmingLocations;
  private MapLocation idealPastrLocation;
  private MapLocation idealNoisetowerLocation;
  private MapLocation defensiveRallyLocation;

  private static final double EPSILON = 1.0e-4;
  private static final double FRIENDLY_FIRE_PENALTY = 25.0;
  
  private static final int LATE_GAME = 1000;
  private static final double WORRISOME_AMOUNT = 0.2;

  public HeadquartersBot(RobotController rc) {
    super(rc);
    this.MY_LOCATION = rc.getLocation();
    this.COW_GROWTHS = rc.senseCowGrowth();
    this.MAP_WIDTH = rc.getMapWidth();
    this.MAP_HEIGHT = rc.getMapHeight();

    this.spawnSquares = computeSpawnSquares();
    this.sensorInfo = new SensorInfo(rc);

    this.computedFarmingLocations = false;
    this.lastAttackPoint = MY_TEAM_HEADQUARTERS;
    
    broadcastAsImmobile();
  }

  public void performRoundActions() throws GameActionException {
    sensorInfo.clear();
    doTeamCommunication();
    updateObjectives();
    clearSpottedEnemies();

    // Spawn if we can.
    if (!rc.isActive()) {
      return;
    }

    if (attack()) {
      return;
    }
    spawn();
  }

  private MapLocation[] computeSpawnSquares() {
    MapLocation[] availableSquares = new MapLocation[Utils.MOVEMENT_DIRECTIONS.length];
    int numberOfAvailableSquares = 0;
    
    Direction toEnemy = MY_TEAM_HEADQUARTERS.directionTo(ENEMY_TEAM_HEADQUARTERS);
    Direction[] directions = new Direction[] {
      toEnemy,
      toEnemy.rotateRight(),
      toEnemy.rotateLeft(),
      toEnemy.rotateRight().rotateRight(),
      toEnemy.rotateLeft().rotateLeft(),
      toEnemy.opposite().rotateLeft(),
      toEnemy.opposite().rotateRight(),
      toEnemy.opposite(),
    };

    for (Direction d : directions) {
      MapLocation candidateSquare = MY_TEAM_HEADQUARTERS.add(d);
      if (Utils.isTraversable(rc, candidateSquare)) {
        availableSquares[numberOfAvailableSquares++] = candidateSquare;
      }
    }

    MapLocation[] toReturn = new MapLocation[numberOfAvailableSquares];
    System.arraycopy(availableSquares, 0, toReturn, 0, numberOfAvailableSquares);
    return toReturn;
  }

  // Returns whether we actually attacked.
  private boolean attack() throws GameActionException {
    sensorInfo.senseEnemyRobots(true);

    if (sensorInfo.getEnemyRobots().length == 0) {
      return false;
    }
    
    sensorInfo.senseMyRobots(true);

    MapLocation bestAttackSquare = null;
    double bestAttackDamage = 0.0;

    for (RobotInfo ri : sensorInfo.getEnemyInfos()) {
      if (ri.location.distanceSquaredTo(MY_LOCATION) > ATTACK_RADIUS_SQUARED) {
        continue;
      }

      double attackDamage = computeAttackDamage(ri.location);
      if (bestAttackDamage + EPSILON < attackDamage) {
        bestAttackDamage = attackDamage;
        bestAttackSquare = ri.location;
      }
    }

    for (RobotInfo ri : sensorInfo.getEnemyInfos()) {
      MapLocation enemyLocation = ri.location;
      Direction d = enemyLocation.directionTo(MY_LOCATION);
      MapLocation candidateTarget = enemyLocation.add(d);
      if (candidateTarget.distanceSquaredTo(MY_LOCATION) > ATTACK_RADIUS_SQUARED) {
        continue;
      }

      double attackDamage = computeAttackDamage(candidateTarget);
      if (bestAttackDamage + EPSILON < attackDamage) {
        bestAttackDamage = attackDamage;
        bestAttackSquare = candidateTarget;
      }
    }

    if (bestAttackSquare != null) {
      rc.attackSquare(bestAttackSquare);
      return true;
    }

    return false;
  }

  private double computeAttackDamage(MapLocation ml) throws GameActionException {
    double attackDamage = 0.0;
    for (RobotInfo ri : sensorInfo.getEnemyInfos()) {
      if (ri.type == RobotType.HQ) {
        // The headquarters takes no damage.
        continue;
      }
      
      if (ri.location.equals(ml)) {
        attackDamage += RobotType.HQ.attackPower;
      } else if (ri.location.isAdjacentTo(ml)) {
        attackDamage += RobotType.HQ.splashPower;
      }
    }
    for (RobotInfo ri : sensorInfo.getMyInfos()) {
      if (ri.type == RobotType.HQ) {
        // The headquarters takes no damage.
        continue;
      }
      
      if (ri.location.equals(ml)) {
        attackDamage -= (RobotType.HQ.attackPower + FRIENDLY_FIRE_PENALTY);
      } else if (ri.location.isAdjacentTo(ml)) {
        attackDamage -= (RobotType.HQ.splashPower + FRIENDLY_FIRE_PENALTY);
      }
    }
    return attackDamage;
  }

  private boolean spawn() throws GameActionException {
    if (rc.senseRobotCount() < GameConstants.MAX_ROBOTS) {
      for (MapLocation ml : spawnSquares) {
        if (rc.senseObjectAtLocation(ml) == null) {
          rc.spawn(MY_TEAM_HEADQUARTERS.directionTo(ml));
          return true;
        }
      }
    }
    return false;
  }

  private int getNumberOfSoldiers() throws GameActionException {
    int count = 0;
    Robot[] myRobots = rc.senseNearbyGameObjects(Robot.class, MAP_WIDTH * MAP_WIDTH + MAP_HEIGHT * MAP_HEIGHT, MY_TEAM);
    for (Robot r : myRobots) {
      RobotInfo ri = rc.senseRobotInfo(r);
      if (ri.type == RobotType.SOLDIER) {
        count++;
      }
    }
    return count;
  }

  private static final int STEPS = 3;
  private MapLocation computeDefensiveRallyLocation() throws GameActionException {
    MapLocation location = idealPastrLocation;
    for (int i = 0; i < STEPS; i++) {
      Direction directionToEnemyHeadquarters = location.directionTo(ENEMY_TEAM_HEADQUARTERS);
      Direction[] plausibleDirections = new Direction[] {
        directionToEnemyHeadquarters,
        directionToEnemyHeadquarters.rotateLeft(),
        directionToEnemyHeadquarters.rotateRight(),
        directionToEnemyHeadquarters.rotateLeft().rotateLeft(),
        directionToEnemyHeadquarters.rotateRight().rotateRight(),
      };

      for (Direction d : plausibleDirections) {
        if (Utils.isTraversable(rc, location.add(d))) {
          location = location.add(d);
        }
      }
    }
    return location;
  }

  private static final int GRAIN_HALF_SIZE = 3;
  private static final double COW_GROWTH_EPSILON = 0.1;
  private static final int STEPS_PER_ITERATION = 20;

  private class ComputationState {
    boolean computedCowGrowthSums;
    // Sum of all cow growths up to and including the given square.
    double[][] cowGrowthSums;

    // Loop variables.
    int x;
    int y;

    boolean computedBestSquare;
    int bestX;
    int bestY;
    double bestCowGrowthSum;
  }

  private ComputationState computationState = null;

  public boolean performLongComputation() throws GameActionException {
    if (computedFarmingLocations) {
      return false;
    }

    if (computationState == null) {
      computationState = new ComputationState();
      computationState.computedCowGrowthSums = false;
      computationState.cowGrowthSums = new double[MAP_WIDTH][MAP_HEIGHT];

      computationState.x = 0;
      computationState.y = 0;

      computationState.computedBestSquare = false;
      computationState.bestX = 0;
      computationState.bestY = 0;
      computationState.bestCowGrowthSum = 0.0;
    }

    if (!computationState.computedCowGrowthSums) {
      for (int i = 0; i < STEPS_PER_ITERATION; i++) {
        int x = computationState.x;
        int y = computationState.y;
        double[][] cowGrowthSums = computationState.cowGrowthSums;

        if (x < MAP_WIDTH) {
          cowGrowthSums[x][y] = 0.0;
          if (Utils.isTraversable(rc, new MapLocation(x, y))) {
            cowGrowthSums[x][y] += COW_GROWTHS[x][y];
          }
          if (x > 0) {
            cowGrowthSums[x][y] += cowGrowthSums[x-1][y];
          }
          if (y > 0) {
            cowGrowthSums[x][y] += cowGrowthSums[x][y-1];
          }
          if (x > 0 && y > 0) {
            cowGrowthSums[x][y] -= cowGrowthSums[x-1][y-1];
          }

          y++;
          if (y >= MAP_HEIGHT) {
            y = 0;
            x++;
          }

          computationState.x = x;
          computationState.y = y;
        } else {
          computationState.computedCowGrowthSums = true;
          computationState.x = 0;
          computationState.y = 0;
          break;
        }
      }
      return true;
    }
    
    if (!computationState.computedBestSquare) {
      int x = computationState.x;
      int y = computationState.y;
      double[][] cowGrowthSums = computationState.cowGrowthSums;
      
      if (x < MAP_WIDTH) {
        if (x - GRAIN_HALF_SIZE >= -1 && x + GRAIN_HALF_SIZE - 1 < MAP_WIDTH &&
            y - GRAIN_HALF_SIZE >= -1 && y + GRAIN_HALF_SIZE - 1 < MAP_HEIGHT) {
          double cowGrowth = cowGrowthSums[x + GRAIN_HALF_SIZE - 1][y + GRAIN_HALF_SIZE - 1];
          if (y - GRAIN_HALF_SIZE >= 0) {
            cowGrowth -= cowGrowthSums[x + GRAIN_HALF_SIZE - 1][y - GRAIN_HALF_SIZE];
          }
          if (x - GRAIN_HALF_SIZE >= 0) {
            cowGrowth -= cowGrowthSums[x - GRAIN_HALF_SIZE][y + GRAIN_HALF_SIZE - 1];
          }
          if (x - GRAIN_HALF_SIZE >= 0 && y - GRAIN_HALF_SIZE >= 0) {
            cowGrowth += cowGrowthSums[x - GRAIN_HALF_SIZE][y - GRAIN_HALF_SIZE];
          }
          
          boolean isBetter = false;
          if (computationState.bestCowGrowthSum < cowGrowth - COW_GROWTH_EPSILON) {
            isBetter = true;
          } else if (Math.abs(computationState.bestCowGrowthSum - cowGrowth) <= COW_GROWTH_EPSILON &&
                     MY_LOCATION.distanceSquaredTo(new MapLocation(x, y)) <
                     MY_LOCATION.distanceSquaredTo(new MapLocation(computationState.bestX, computationState.bestY))) {
            isBetter = true;
          }

          if (isBetter) {
            computationState.bestCowGrowthSum = cowGrowth;
            computationState.bestX = x;
            computationState.bestY = y;
          }
        }

        y++;
        if (y >= MAP_HEIGHT) {
          y = 0;
          x++;
        }

        computationState.x = x;
        computationState.y = y;
        return true;
      } else {
        computationState.computedBestSquare = true;
        return true;
      }
    }

    // Done computing.
    {
      idealPastrLocation = findNearbyTraversableSquare(new MapLocation(computationState.bestX, computationState.bestY), null);
      idealNoisetowerLocation = findNearbyTraversableSquare(idealPastrLocation, idealPastrLocation);
      defensiveRallyLocation = computeDefensiveRallyLocation();

      communication.sendPastrLocation(idealPastrLocation);
      communication.sendNoisetowerLocation(idealNoisetowerLocation);
      communication.sendComputationsDone(true);
      computedFarmingLocations = true;
      return false;
    }
  }

  private MapLocation findNearbyTraversableSquare(MapLocation center, MapLocation squareToAvoid) throws GameActionException {
    MapLocation testLocation;
    for (int offset = 0; ; offset++) {
      for (int x = center.x - offset; x <= center.x + offset; x++) {
        testLocation = new MapLocation(x, center.y - offset);
        if (Utils.isTraversable(rc, testLocation) && !testLocation.equals(squareToAvoid) && !testLocation.equals(MY_TEAM_HEADQUARTERS)) {
          return testLocation;
        }

        testLocation = new MapLocation(x, center.y - offset);
        if (Utils.isTraversable(rc, testLocation) && !testLocation.equals(squareToAvoid) && !testLocation.equals(MY_TEAM_HEADQUARTERS)) {
          return testLocation;
        }
      }

      for (int y = center.y - offset; y <= center.y + offset; y++) {
        testLocation = new MapLocation(center.x - offset, y);
        if (Utils.isTraversable(rc, testLocation) && !testLocation.equals(squareToAvoid) && !testLocation.equals(MY_TEAM_HEADQUARTERS)) {
          return testLocation;
        }
        
        testLocation = new MapLocation(center.x + offset, y);
        if (Utils.isTraversable(rc, testLocation) && !testLocation.equals(squareToAvoid) && !testLocation.equals(MY_TEAM_HEADQUARTERS)) {
          return testLocation;
        }
      }
    }
  }

  private static final int SIZABLE_ARMY = 8;

  private void doTeamCommunication() throws GameActionException {
    if (computedFarmingLocations && !communication.receiveFarmSignal()) {
      boolean shouldSendSignal = false;
      if (rc.senseNearbyGameObjects(Robot.class, ENEMY_TEAM_HEADQUARTERS, 50, MY_TEAM).length >= SoldierBot.FARMING_QUOTA) {
        shouldSendSignal = true;
      } else if (Clock.getRoundNum() >= LATE_GAME) {
        shouldSendSignal = true;
      } else if (rc.senseTeamMilkQuantity(ENEMY_TEAM) > GameConstants.WIN_QTY * WORRISOME_AMOUNT) {
        shouldSendSignal = true;
      } else if (rc.sensePastrLocations(ENEMY_TEAM).length > 0 &&
                 rc.senseNearbyGameObjects(Robot.class, MAP_WIDTH * MAP_WIDTH + MAP_HEIGHT * MAP_HEIGHT, MY_TEAM).length >= SIZABLE_ARMY) {
        shouldSendSignal = true;
      }

      if (shouldSendSignal) {
        sendFarmingSignal();
      }
    }

    Robot[] myRobots = null;

    int noisetowerBuilderId = communication.receiveNoisetowerBuilderId();

    if (noisetowerBuilderId != Communication.NOBODY) {
      if (myRobots == null) {
        myRobots = rc.senseNearbyGameObjects(
                Robot.class,
                GameConstants.MAP_MAX_WIDTH * GameConstants.MAP_MAX_WIDTH + GameConstants.MAP_MAX_HEIGHT * GameConstants.MAP_MAX_HEIGHT,
                MY_TEAM);
      }
      boolean foundNoisetowerBuilder = false;
      for (Robot r : myRobots) {
        if (r.getID() == noisetowerBuilderId) {
          foundNoisetowerBuilder = true;
          break;
        }
      }

      if (!foundNoisetowerBuilder) {
        communication.sendNoisetowerBuilderId(Communication.NOBODY);
      }
    }

    int defendersClaimed = communication.receivePastrDefendersClaimed();
    if (defendersClaimed > 0) {
      if (myRobots == null) {
        myRobots = rc.senseNearbyGameObjects(
                Robot.class,
                GameConstants.MAP_MAX_WIDTH * GameConstants.MAP_MAX_WIDTH + GameConstants.MAP_MAX_HEIGHT * GameConstants.MAP_MAX_HEIGHT,
                MY_TEAM);
      }
      int[] pastrDefenderIds = new int[defendersClaimed];
      for (int i = 0; i < pastrDefenderIds.length; i++) {
        pastrDefenderIds[i] = communication.receivePastrDefenderId(i);
      }

      int oldDefendersClaimed = defendersClaimed;

      for (int i = pastrDefenderIds.length; --i >= 0; ) {
        boolean found = false;
        for (Robot r : myRobots) {
          if (r.getID() == pastrDefenderIds[i]) {
            found = true;
            break;
          }
        }
        if (!found) {
          int temp = pastrDefenderIds[defendersClaimed - 1];
          pastrDefenderIds[defendersClaimed - 1] = pastrDefenderIds[i];
          pastrDefenderIds[i] = temp;

          defendersClaimed--;
        }
      }

      if (defendersClaimed < oldDefendersClaimed) {
        for (int i = 0; i < defendersClaimed; i++) {
          communication.sendPastrDefenderId(i, pastrDefenderIds[i]);
        }
        communication.sendPastrDefendersClaimed(defendersClaimed);
      }
    }
  }

  private MapLocation getRallyPoint() throws GameActionException {
    if (firstEnemyPastr != null) {
      return firstEnemyPastr;
    }
    if (defensiveRallyLocation != null) {
      return defensiveRallyLocation;
    }
    return new MapLocation((ENEMY_TEAM_HEADQUARTERS.x + MY_TEAM_HEADQUARTERS.x) / 2,
                           (ENEMY_TEAM_HEADQUARTERS.y + MY_TEAM_HEADQUARTERS.y) / 2);
  }

  private MapLocation lastAttackPoint;
  private int lastAttackPointRound;
  private static final int CONSERVATION_ROUNDS = 10;
  private static final int THRESHOLD_DISTANCE = 50;

  private MapLocation firstEnemyPastr = null;

  private void updateObjectives() throws GameActionException {
    // Go for the pastr furthest away from the enemy headquarters, or
    // surround the enemy headquarters, if there is no target.
    Objective bestObjective = new Objective(ObjectiveType.RALLY, getRallyPoint());
    Objective currentObjective = communication.receiveObjective();
    MapLocation[] enemyPastrs = rc.sensePastrLocations(ENEMY_TEAM);

    if (enemyPastrs.length == 1) {
      if (firstEnemyPastr == null) {
        firstEnemyPastr = enemyPastrs[0];
      }
      bestObjective = new Objective(ObjectiveType.ATTACK, enemyPastrs[0]);
    } else if (enemyPastrs.length > 1) {
      double bestSum = -1.0;

      Robot[] myRobots = rc.senseNearbyGameObjects(Robot.class, MAP_WIDTH * MAP_WIDTH + MAP_HEIGHT * MAP_HEIGHT, MY_TEAM);
      RobotInfo[] myInfos = new RobotInfo[myRobots.length];

      for (int i = 0; i < myRobots.length; i++) {
        myInfos[i] = rc.senseRobotInfo(myRobots[i]);
      }

      for (int i = 0; i < enemyPastrs.length; i++) {
        double sum = 0.0;
        if (enemyPastrs[i].distanceSquaredTo(ENEMY_TEAM_HEADQUARTERS) > RobotType.HQ.attackRadiusMaxSquared) {
          for (RobotInfo ri : myInfos) {
            if (ri.type != RobotType.SOLDIER) {
              continue;
            }
            sum += 1.0 / ri.location.distanceSquaredTo(enemyPastrs[i]);
          }
        }
        if (bestSum < sum) {
          bestSum = sum;
          bestObjective = new Objective(ObjectiveType.DESTROY, enemyPastrs[i]);
        }
      }
    } else {
      Robot[] enemyRobots = rc.senseNearbyGameObjects(Robot.class, SENSOR_RADIUS_SQUARED, ENEMY_TEAM);
      MapLocation newTarget = null;
      if (enemyRobots.length > 0) {
        RobotInfo enemyInfo = rc.senseRobotInfo(enemyRobots[0]);
        newTarget = enemyInfo.location;
      } else {
        int numberOfSpottedEnemies = communication.receiveNumberOfSpottedEnemies();
        for (int i = 0; i < numberOfSpottedEnemies; i++) {
          MapLocation spottedEnemy = communication.receiveSpottedEnemy(i);
          if (newTarget == null || lastAttackPoint.distanceSquaredTo(newTarget) < lastAttackPoint.distanceSquaredTo(spottedEnemy)) {
            newTarget = spottedEnemy;
          }
        }
      }
      if (newTarget != null) {
        if (currentObjective == null || currentObjective.type == ObjectiveType.RALLY ||
             (newTarget.distanceSquaredTo(lastAttackPoint) >= THRESHOLD_DISTANCE &&
             Clock.getRoundNum() - lastAttackPointRound >= CONSERVATION_ROUNDS)) {
          bestObjective = new Objective(ObjectiveType.ATTACK, newTarget);
          lastAttackPoint = newTarget;
          lastAttackPointRound = Clock.getRoundNum();
        } else if (newTarget.distanceSquaredTo(lastAttackPoint) < THRESHOLD_DISTANCE) {
          lastAttackPointRound = Clock.getRoundNum();
        }
      }
    }

    if (bestObjective.type == ObjectiveType.RALLY && currentObjective != null &&
        currentObjective.type == ObjectiveType.ATTACK &&
        Clock.getRoundNum() - lastAttackPointRound < CONSERVATION_ROUNDS) {
      return;
    }

    updateObjective(bestObjective, true);
  }

  private void clearSpottedEnemies() throws GameActionException {
    communication.sendNumberOfSpottedEnemies(0);
  }
}
