package team213.debugging;

/**
 * Something similar to the Runnable interface.
 *
 * @author kxing
 */
public interface DebugFunction {

  public void run();
}
