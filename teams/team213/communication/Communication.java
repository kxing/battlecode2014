package team213.communication;

import battlecode.common.*;
import team213.debugging.*;
import team213.utils.*;

/**
 *
 * @author kxing
 */
public class Communication {

  private final RobotController rc;

  private static final int FARM_CHANNEL = 0;
  private static final int COMPUTATIONS_DONE_CHANNEL = 1;
  private static final int PASTR_LOCATION_CHANNEL = 2;
  private static final int NOISETOWER_CHANNEL = 3;

  private static final int NOISETOWER_BUILDER_CHANNEL = 4;

  private static final int OBJECTIVE_CHANNEL = 5;

  private static final int NUMBER_OF_SPOTTED_ENEMIES_CHANNEL = 6;
  
  // Channel 7 is unused so far.

  private static final int PASTR_DEFENDERS_NECESSARY_CHANNEL = 8;
  private static final int PASTR_DEFENDERS_CLAIMED_CHANNEL = 9;
  private static final int PASTR_DEFENDERS_OFFSET = 10;

  // Channels 10-24 are used for recording pastr defender IDs.

  private static final int SPOTTED_ENEMIES_OFFSET = 25;

  // Channels 25-49 are used for recording the IDs of spotted enemies.

  // Channels 50-9999 are unused so far.
  private static final int MAP_OFFSET = 10000;

  // Channels 20000+ are unused so far.
  
  public static final int NOBODY = 0;

  public Communication(RobotController rc) {
    this.rc = rc;
  }

  public void sendFarmSignal(boolean shouldFarm) throws GameActionException {
    rc.broadcast(FARM_CHANNEL, booleanToInt(shouldFarm));
  }

  public boolean receiveFarmSignal() throws GameActionException {
    int data = rc.readBroadcast(FARM_CHANNEL);
    return intToBoolean(data);
  }

  public void sendComputationsDone(boolean computationsDone) throws GameActionException {
    rc.broadcast(COMPUTATIONS_DONE_CHANNEL, booleanToInt(computationsDone));
  }

  public boolean receiveComputationsDone() throws GameActionException {
    int data = rc.readBroadcast(COMPUTATIONS_DONE_CHANNEL);
    return intToBoolean(data);
  }

  public void sendPastrLocation(MapLocation pastrLocation) throws GameActionException {
    rc.broadcast(PASTR_LOCATION_CHANNEL, mapLocationToInt(pastrLocation));
  }

  public MapLocation receivePastrLocation() throws GameActionException {
    int data = rc.readBroadcast(PASTR_LOCATION_CHANNEL);
    return intToMapLocation(data);
  }

  public void sendNoisetowerLocation(MapLocation noisetowerLocation) throws GameActionException {
    rc.broadcast(NOISETOWER_CHANNEL, mapLocationToInt(noisetowerLocation));
  }

  public MapLocation receiveNoisetowerLocation() throws GameActionException {
    int data = rc.readBroadcast(NOISETOWER_CHANNEL);
    return intToMapLocation(data);
  }

  public void sendNoisetowerBuilderId(int noisetowerBuilderId) throws GameActionException {
    rc.broadcast(NOISETOWER_BUILDER_CHANNEL, noisetowerBuilderId);
  }

  public int receiveNoisetowerBuilderId() throws GameActionException {
    return rc.readBroadcast(NOISETOWER_BUILDER_CHANNEL);
  }

  public void sendObjective(Objective objective) throws GameActionException {
    if (objective == null) {
      rc.broadcast(OBJECTIVE_CHANNEL, 0);
      return;
    }
    rc.broadcast(OBJECTIVE_CHANNEL, objectiveToInt(objective));
  }

  public Objective receiveObjective() throws GameActionException {
    return intToObjective(rc.readBroadcast(OBJECTIVE_CHANNEL));
  }

  public void sendNumberOfSpottedEnemies(int numberOfSpottedEnemies) throws GameActionException {
    rc.broadcast(NUMBER_OF_SPOTTED_ENEMIES_CHANNEL, numberOfSpottedEnemies);
  }

  public int receiveNumberOfSpottedEnemies() throws GameActionException {
    return rc.readBroadcast(NUMBER_OF_SPOTTED_ENEMIES_CHANNEL);
  }

  public void sendSpottedEnemy(MapLocation location, int index) throws GameActionException {
    rc.broadcast(SPOTTED_ENEMIES_OFFSET + index, mapLocationToInt(location));
  }

  public MapLocation receiveSpottedEnemy(int index) throws GameActionException {
    int data = rc.readBroadcast(SPOTTED_ENEMIES_OFFSET + index);
    return intToMapLocation(data);
  }

  public void sendPastrDefendersNecessary(int defendersNecessary) throws GameActionException {
    rc.broadcast(PASTR_DEFENDERS_NECESSARY_CHANNEL, defendersNecessary);
  }

  public int receivePastrDefendersNecessary() throws GameActionException {
    return rc.readBroadcast(PASTR_DEFENDERS_NECESSARY_CHANNEL);
  }

  public void sendPastrDefendersClaimed(int defendersClaimed) throws GameActionException {
    rc.broadcast(PASTR_DEFENDERS_CLAIMED_CHANNEL, defendersClaimed);
  }

  public int receivePastrDefendersClaimed() throws GameActionException {
    return rc.readBroadcast(PASTR_DEFENDERS_CLAIMED_CHANNEL);
  }

  public void sendPastrDefenderId(int index, int id) throws GameActionException {
    Assert.debug_Assert(index < GameConstants.MAX_ROBOTS);
    rc.broadcast(PASTR_DEFENDERS_OFFSET + index, id);
  }

  public int receivePastrDefenderId(int index) throws GameActionException {
    return rc.readBroadcast(PASTR_DEFENDERS_OFFSET + index);
  }

  public void sendDoNothing(MapLocation ml, int id) throws GameActionException {
    sendDataForMapLocation(ml, id);
  }

  public int receiveDoNothing(MapLocation ml) throws GameActionException {
    return receiveDataForMapLocation(ml);
  }

  // Internal conversion methods.

  private static final int MAP_LOCATION_X_MULTIPLIER = 1000;

  private static int mapLocationToInt(MapLocation ml) {
    return ml.x * MAP_LOCATION_X_MULTIPLIER + ml.y;
  }

  private static MapLocation intToMapLocation(int data) {
    return new MapLocation(data / MAP_LOCATION_X_MULTIPLIER, data % MAP_LOCATION_X_MULTIPLIER);
  }

  private static final int YES = 1;
  private static final int NO = 0;

  private static int booleanToInt(boolean b) {
    return b ? YES : NO;
  }

  private static boolean intToBoolean(int data) {
    return (data == YES);
  }

  private static int mapLocationToChannel(MapLocation ml) {
    return MAP_OFFSET + (ml.x * GameConstants.MAP_MAX_HEIGHT) + ml.y;
  }

  private static final int OBJECTIVE_TYPE_MULTIPLIER = 1000000;

  private static int objectiveTypeToInt(ObjectiveType objectiveType) {
    switch (objectiveType) {
      case ATTACK:
        return 1;
      case DESTROY:
        return 2;
      case RALLY:
        return 3;
      default:
        Assert.debug_Assert(false);
        break;
    }
    return 0;
  }

  private static ObjectiveType intToObjectiveType(int data) {
    switch (data) {
      case 1:
        return ObjectiveType.ATTACK;
      case 2:
        return ObjectiveType.DESTROY;
      case 3:
        return ObjectiveType.RALLY;
      default:
        break;
    }
    return null;
  }

  private static int objectiveToInt(Objective objective) {
    return objectiveTypeToInt(objective.type) * OBJECTIVE_TYPE_MULTIPLIER +
           mapLocationToInt(objective.target);
  }

  private static Objective intToObjective(int data) {
    ObjectiveType type = intToObjectiveType(data / OBJECTIVE_TYPE_MULTIPLIER);
    if (type == null) {
      return null;
    }
    return new Objective(type,
                         intToMapLocation(data % OBJECTIVE_TYPE_MULTIPLIER));
  }

  // Map broadcast methods.

  private void sendDataForMapLocation(MapLocation ml, int data) throws GameActionException {
    rc.broadcast(mapLocationToChannel(ml), data);
  }

  private int receiveDataForMapLocation(MapLocation ml) throws GameActionException {
    return rc.readBroadcast(mapLocationToChannel(ml));
  }
}
