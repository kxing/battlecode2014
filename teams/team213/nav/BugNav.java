package team213.nav;

import battlecode.common.*;
import java.util.*;
import team213.communication.*;
import team213.utils.Utils;

/**
 * Bug navigation.
 *
 * @author kxing
 */
public class BugNav {
  private RobotController rc;
  private Communication communication;

  private MapLocation goal;

  private boolean isBugging;
  private int buggingMoves;
  private int minimumDistanceWhileBugging;
  private Direction lastDirection;
  private boolean nextBugCounterclockwise;

  private MapLocation wallPoint;

  private HashSet<MapLocation> visited;

  public BugNav(RobotController rc, Communication communication) {
    this.rc = rc;
    this.communication = communication;
    this.isBugging = false;
    this.buggingMoves = 0;
    this.nextBugCounterclockwise = true;
    this.visited = new HashSet<MapLocation>();

    // The initial last direction is arbitrary, but shouldn't matter.
    this.lastDirection = Direction.NORTH;
  }

  public void setGoal(MapLocation goal) {
    // TODO(kxing): Make this logic more watertight.
    if (!goal.equals(this.goal)) {
      nextBugCounterclockwise = true;
      visited.clear();
      buggingMoves = 0;
    }
    this.goal = goal;
    isBugging = false;
  }

  private boolean tryMove(Direction d, DirectionEvaluations directionEvaluation, boolean shouldSneak) throws GameActionException {
    if (!rc.canMove(d)) {
      return false;
    }
    if (directionEvaluation.isDangerous(d)) {
      return false;
    }
    if (shouldSneak) {
      rc.sneak(d);
    } else {
      rc.move(d);
    }
    lastDirection = d;
    return true;
  }

  private static final int FOREVER = 100;

  public boolean moveTowardsGoal(DirectionEvaluations directionEvaluations, boolean shouldSneak) throws GameActionException {
    if (!rc.isActive()) {
      return false;
    }
    if (rc.getLocation().equals(goal)) {
      return true;
    }
    MapLocation myLocation = rc.getLocation();
    Direction goalDirection = myLocation.directionTo(goal);

    if (!isBugging) {
      if (tryMove(goalDirection, directionEvaluations, shouldSneak)) {
        return true;
      }
      if (tryMove(goalDirection.rotateLeft(), directionEvaluations, shouldSneak)) {
        return true;
      }
      if (tryMove(goalDirection.rotateRight(), directionEvaluations, shouldSneak)) {
        return true;
      }
      MapLocation obstacleLocation = myLocation.add(goalDirection);
      if (!isWall(obstacleLocation, directionEvaluations)) {
        // Someone else is in our way - it's not a wall.
        return bugMove(true, directionEvaluations, shouldSneak);
      }
      isBugging = true;
      buggingMoves = 0;
      wallPoint = obstacleLocation;

      nextBugCounterclockwise = decideBuggingDirection();

      minimumDistanceWhileBugging = myLocation.distanceSquaredTo(goal);
    }

    // See if we can stop bugging.
    if (!visited.contains(myLocation)
            && !Utils.areDirectionsWithin45(goalDirection, lastDirection.opposite())
            && myLocation.add(goalDirection).distanceSquaredTo(goal) < minimumDistanceWhileBugging
            && tryMove(goalDirection, directionEvaluations, shouldSneak)) {
      isBugging = false;
      buggingMoves = 0;
      visited.add(myLocation);
      return true;
    }

    if (minimumDistanceWhileBugging > myLocation.distanceSquaredTo(goal)) {
      minimumDistanceWhileBugging = myLocation.distanceSquaredTo(goal);
    }

    buggingMoves++;
    // If our old wall is no longer a wall, or if we've been buggin forever, reset.
    if (!isWall(wallPoint, directionEvaluations) || buggingMoves >= FOREVER) {
      isBugging = false;
      visited.clear();
      return false;
    }

    // Go back to the wall, if we aren't at the wall.
    if (!myLocation.isAdjacentTo(wallPoint)) {
      Direction directionToWall = myLocation.directionTo(wallPoint);

      if (tryMove(directionToWall, directionEvaluations, shouldSneak)) {
        // In case the obstacle was a bot that moved out of the way.
        return true;
      }
    }

    return bugMove(nextBugCounterclockwise, directionEvaluations, shouldSneak);
  }

  private boolean bugMove(boolean isCounterclockwise, DirectionEvaluations directionEvaluations, boolean shouldSneak) throws GameActionException {
    MapLocation myLocation = rc.getLocation();
    Direction directionToWall;
    if (isBugging) {
      directionToWall = myLocation.directionTo(wallPoint);
    } else {
      directionToWall = myLocation.directionTo(goal);
    }
    Direction goalDirection = myLocation.directionTo(goal);

    Direction testDirection;

    if (isCounterclockwise) {
      testDirection = directionToWall.rotateRight();
    } else {
      testDirection = directionToWall.rotateLeft();
    }

    boolean canUpdateWall = true;

    do {
      if (tryMove(testDirection, directionEvaluations, shouldSneak)) {
        if (isBugging &&
            (rc.senseTerrainTile(wallPoint) == TerrainTile.OFF_MAP ||
             rc.senseTerrainTile(wallPoint.add(directionToWall)) == TerrainTile.OFF_MAP)) {
          if (Utils.areDirectionsWithin45(goalDirection, lastDirection.opposite())) {
            nextBugCounterclockwise = !nextBugCounterclockwise;
            buggingMoves = 0;
          }
        }
        return true;
      }
      MapLocation occupiedSquare = myLocation.add(testDirection);
      if (isBugging && myLocation.isAdjacentTo(wallPoint) && canUpdateWall && isWall(occupiedSquare, directionEvaluations)) {
        wallPoint = occupiedSquare;
      } else {
        canUpdateWall = false;
      }
      if (nextBugCounterclockwise) {
        testDirection = testDirection.rotateRight();
      } else {
        testDirection = testDirection.rotateLeft();
      }
    } while (testDirection != directionToWall);

    return false;
  }

  private boolean isWall(MapLocation ml, DirectionEvaluations directionEvaluation) throws GameActionException {
    if (!Utils.isTraversable(rc, ml)) {
      return true;
    }
    MapLocation myLocation = rc.getLocation();
    if (myLocation.isAdjacentTo(ml) && directionEvaluation.isDangerous(myLocation.directionTo(ml))) {
      return true;
    }
    MapLocation enemyHeadquarters = rc.senseEnemyHQLocation();
    Direction enemyHeadquartersDirection = ml.directionTo(enemyHeadquarters);
    if (ml.add(enemyHeadquartersDirection).distanceSquaredTo(enemyHeadquarters) <= RobotType.HQ.attackRadiusMaxSquared) {
      return true;
    }
    int id = communication.receiveDoNothing(ml);
    if (id == Communication.NOBODY) {
      return false;
    }
    if (!rc.canSenseSquare(ml)) {
      return false;
    }
    GameObject go = rc.senseObjectAtLocation(ml);
    return (go != null && go.getID() == id);
  }

  private static final int DECISION_ITERATIONS = 10;

  private boolean decideBuggingDirection() throws GameActionException {
    MapLocation counterclockwiseWallPoint = wallPoint;
    MapLocation counterclockwiseLocation = rc.getLocation();
    int minimumCounterclockwiseDistance = counterclockwiseLocation.distanceSquaredTo(goal);
    int minimumCounterclockwiseIterations = 0;

    MapLocation clockwiseWallPoint = wallPoint;
    MapLocation clockwiseLocation = rc.getLocation();
    int minimumClockwiseDistance = clockwiseLocation.distanceSquaredTo(goal);
    int minimumClockwiseIterations = 0;

    for (int i = 0; i < DECISION_ITERATIONS; i++) {
      // Fake bug in the counterclockwise direction.
      Direction initialCounterclockwiseDirection = counterclockwiseLocation.directionTo(counterclockwiseWallPoint);
      Direction counterclockwiseDirection = initialCounterclockwiseDirection;
      do {
        counterclockwiseDirection = counterclockwiseDirection.rotateRight();
      } while (!Utils.isTraversable(rc, counterclockwiseLocation.add(counterclockwiseDirection)) &&
               counterclockwiseDirection != initialCounterclockwiseDirection);
      if (counterclockwiseDirection != initialCounterclockwiseDirection) {
        counterclockwiseLocation = counterclockwiseLocation.add(counterclockwiseDirection);

        int newDistance = counterclockwiseLocation.distanceSquaredTo(goal);
        if (minimumCounterclockwiseDistance > newDistance) {
          minimumCounterclockwiseDistance = newDistance;
          minimumCounterclockwiseIterations = i+1;
        }
      }

      // Fake bug in the clockwise direction.
      Direction initialClockwiseDirection = clockwiseLocation.directionTo(clockwiseWallPoint);
      Direction clockwiseDirection = initialClockwiseDirection;
      do {
        clockwiseDirection = clockwiseDirection.rotateLeft();
      } while (!Utils.isTraversable(rc, clockwiseLocation.add(clockwiseDirection)) &&
               clockwiseDirection != initialClockwiseDirection);
      if (clockwiseDirection != initialClockwiseDirection) {
        clockwiseLocation = clockwiseLocation.add(clockwiseDirection);

        int newDistance = clockwiseLocation.distanceSquaredTo(goal);
        if (minimumClockwiseDistance > newDistance) {
          minimumClockwiseDistance = newDistance;
          minimumClockwiseIterations = i+1;
        }
      }
    }

    if (minimumCounterclockwiseDistance <= minimumClockwiseDistance ||
        (minimumCounterclockwiseDistance == minimumClockwiseDistance &&
         minimumCounterclockwiseIterations < minimumClockwiseIterations)) {
      return true;
    } else {
      return false;
    }
  }
}
