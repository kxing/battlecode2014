package team213.utils;

/**
 *
 * @author kxing
 */
public enum ObjectiveType {
  DESTROY,
  ATTACK,
  RALLY,
}
