package team213.utils;

import battlecode.common.*;

/**
 *
 * @author kxing
 */
public class Objective {
  public final ObjectiveType type;
  public final MapLocation target;
  
  public Objective(ObjectiveType type, MapLocation target) {
    this.type = type;
    this.target = target;
  }

  public boolean equals(Objective objective) {
    return (type == objective.type && target.equals(objective.target));
  }
}
