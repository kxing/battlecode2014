package turtle;

import battlecode.common.*;

/**
 * Main entry point.
 * 
 * @author kxing
 */
public class RobotPlayer {
	
	public static void run(RobotController rc) {
    while (true) {
      try {
        switch(rc.getType()) {
          case SOLDIER:
            SoldierCode.runSoldier(rc);
            break;
          case HQ:
            HeadquartersCode.runHq(rc);
            break;
          case PASTR:
            PastrCode.runPastr(rc);
            break;
          case NOISETOWER:
            NoisetowerCode.runNoisetower(rc);
            break;
          default:
            System.err.println(String.format("Invalid type detected: %s",
                    rc.getType().toString()));
        }
      } catch (Exception e) {
        e.printStackTrace();
      }
    }
	}

  public static class SoldierCode {
    public static void runSoldier(RobotController rc) throws GameActionException {
      if (rc.isActive()) {
        if (Clock.getRoundNum() < GameConstants.HQ_SPAWN_DELAY_CONSTANT_1) {
          rc.construct(RobotType.NOISETOWER);
        } else if (Clock.getRoundNum() < GameConstants.HQ_SPAWN_DELAY_CONSTANT_1 * 2) {
          rc.construct(RobotType.PASTR);
        }
      }

      rc.yield();
    }
  }

  public static class HeadquartersCode {
    private static final int SOLDIERS = 2;

    public static void runHq(RobotController rc) throws GameActionException {
      // Spawn if we can.
      if (rc.isActive()) {
        if (!attack(rc)) {
          // Spawn if we don't attack.
          // Good enough approximation.
          if (Clock.getRoundNum() < GameConstants.HQ_SPAWN_DELAY_CONSTANT_1 * SOLDIERS) {
            spawn(rc);
          }
        }
      }

      rc.yield();
    }

    // Returns whether we actually attacked.
    private static boolean attack(RobotController rc) throws GameActionException {
      Robot[] enemyRobots = rc.senseNearbyGameObjects(Robot.class, rc.getType().sensorRadiusSquared, rc.getTeam().opponent());
      RobotInfo[] enemyInfos = new RobotInfo[enemyRobots.length];

      for (int i = 0; i < enemyInfos.length; i++) {
        enemyInfos[i] = rc.senseRobotInfo(enemyRobots[i]);
      }

      MapLocation MY_LOCATION = rc.getLocation();
      int ATTACK_RADIUS_SQUARED = rc.getType().attackRadiusMaxSquared;

      for (RobotInfo ri : enemyInfos) {
        if (ri.location.distanceSquaredTo(MY_LOCATION) <= ATTACK_RADIUS_SQUARED) {
          rc.attackSquare(ri.location);
          return true;
        }
      }

      for (RobotInfo ri : enemyInfos) {
        MapLocation enemyLocation = ri.location;
        Direction d = enemyLocation.directionTo(MY_LOCATION);
        if (enemyLocation.add(d).distanceSquaredTo(MY_LOCATION) <= ATTACK_RADIUS_SQUARED) {
          rc.attackSquare(enemyLocation.add(d));
          return true;
        }
      }

      return false;
    }

    public static boolean spawn(RobotController rc) throws GameActionException {
      if (rc.senseRobotCount() < GameConstants.MAX_ROBOTS) {
        Direction startSpawnDirection = rc.senseEnemyHQLocation().directionTo(rc.getLocation());
        Direction spawnDirection = startSpawnDirection;
        do {
          MapLocation spawnSquare = rc.getLocation().add(spawnDirection);
          if (rc.senseObjectAtLocation(spawnSquare) == null) {
            TerrainTile tile = rc.senseTerrainTile(spawnSquare);
            if (tile != TerrainTile.OFF_MAP && tile != TerrainTile.VOID) {
              rc.spawn(spawnDirection);
              return true;
            }
          }
          spawnDirection = spawnDirection.rotateLeft();
        } while (spawnDirection != startSpawnDirection);
      }
      return false;
    }
  }

  public static class PastrCode {
    public static void runPastr(RobotController rc) throws GameActionException {
      rc.yield();
    }
  }

  public static class NoisetowerCode {
    public static void runNoisetower(RobotController rc) throws GameActionException {
      if (rc.isActive()) {
        shoot(rc);
      }
      rc.yield();
    }

    private static final Direction[] DIRECTIONS = {
      Direction.NORTH,
      Direction.NORTH_EAST,
      Direction.EAST,
      Direction.SOUTH_EAST,
      Direction.SOUTH,
      Direction.SOUTH_WEST,
      Direction.WEST,
      Direction.NORTH_WEST,
    };

    private static int directionIndex = DIRECTIONS.length - 1;
    private static MapLocation targetLocation = null;
    private static MapLocation currentLocation = null;

    private static boolean shoot(RobotController rc) throws GameActionException {
      MapLocation pastrLocation = getPastrLocation(rc);
      if (pastrLocation == null) {
        return false;
      }
      Direction currentDirection = DIRECTIONS[directionIndex];
      if (currentLocation == null || targetLocation.equals(pastrLocation)) {
        directionIndex = (directionIndex + 1) % DIRECTIONS.length;
        currentDirection = DIRECTIONS[directionIndex];
        targetLocation = pastrLocation;
        currentLocation = pastrLocation;

        while (currentLocation.distanceSquaredTo(targetLocation) <= GameConstants.ATTACK_SCARE_RANGE) {
          currentLocation = currentLocation.add(currentDirection);
        }
        currentLocation = currentLocation.add(currentDirection.opposite());

        while (currentLocation.distanceSquaredTo(rc.getLocation()) <= RobotType.NOISETOWER.attackRadiusMaxSquared &&
               rc.senseTerrainTile(targetLocation) != TerrainTile.OFF_MAP) {
          targetLocation = targetLocation.add(currentDirection);
          currentLocation = currentLocation.add(currentDirection);
        }
      }

      currentLocation = currentLocation.add(currentDirection.opposite());
      targetLocation = targetLocation.add(currentDirection.opposite());
      rc.attackSquare(currentLocation);
      return true;
    }
  }

  public static MapLocation getPastrLocation(RobotController rc) throws GameActionException {
    MapLocation[] pastrLocations = rc.sensePastrLocations(rc.getTeam());
    if (pastrLocations.length == 0) {
      return null;
    }
    return pastrLocations[0];
  }

}
