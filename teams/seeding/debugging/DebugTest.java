package seeding.debugging;

/**
 * Convenience class to aid in debugging a bot.
 *
 * @author kxing
 */
public class DebugTest {

  public static void debug_RunAtGivenRound(DebugFunction function) {
    function.run();
  }
}
