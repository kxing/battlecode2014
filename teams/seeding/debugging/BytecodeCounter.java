package seeding.debugging;

import battlecode.common.*;

/**
 * A convenience wrapper for keeping track of bytecodes.
 *
 * @author kxing
 */
public class BytecodeCounter {
  private int startRound;
  private int startBytecodes;
  private boolean running;

  public BytecodeCounter() {
    running = false;
  }

  public void start() {
    assert(!running);
    startRound = Clock.getRoundNum();
    startBytecodes = Clock.getBytecodeNum();
    running = true;
  }

  public int getBytecodesUsed() {
    assert(running);
    int round = Clock.getRoundNum();
    int bytecodes = Clock.getBytecodeNum();
    return (round - startRound) * GameConstants.BYTECODE_LIMIT + (bytecodes - startBytecodes);
  }

  public void printBytecodesUsed(String marker) {
    System.out.println(marker + ": " + getBytecodesUsed());
  }

  public void stop() {
    assert(running);
    running = false;
  }
}
