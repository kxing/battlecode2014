/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package seeding.utils;

import java.util.*;
import battlecode.common.*;

/**
 *
 * @author kxing
 */
public class Utils {

	public static final Random rand;
  public static final Direction[] MOVEMENT_DIRECTIONS = new Direction[] {
    Direction.NORTH,
    Direction.NORTH_EAST,
    Direction.EAST,
    Direction.SOUTH_EAST,
    Direction.SOUTH,
    Direction.SOUTH_WEST,
    Direction.WEST,
    Direction.NORTH_WEST
  };

  public static final Direction[] ATTACK_MICRO_DIRECTIONS = new Direction[] {
    Direction.NORTH,
    Direction.NORTH_EAST,
    Direction.EAST,
    Direction.SOUTH_EAST,
    Direction.SOUTH,
    Direction.SOUTH_WEST,
    Direction.WEST,
    Direction.NORTH_WEST,
    Direction.NONE
  };

  static {
		rand = new Random();
  }

  public static boolean areDirectionsWithin45(Direction d1, Direction d2) {
    return (d1 == d2 || d1 == d2.rotateLeft() || d1 == d2.rotateRight());
  }

  public static boolean isTraversable(RobotController rc, MapLocation ml) {
    switch (rc.senseTerrainTile(ml)) {
      case NORMAL:
      case ROAD:
        return true;
      case VOID:
      case OFF_MAP:
        return false;
      default:
        return true;
    }
  }
}
