package seeding.nav;

import battlecode.common.*;
import java.util.*;
import seeding.utils.Utils;

/**
 * Bug navigation.
 *
 * @author kxing
 */
public class BugNav {
  private RobotController rc;

  private MapLocation goal;

  private boolean isBugging;
  private int minimumDistanceWhileBugging;
  private Direction lastDirection;
  private boolean nextBugCounterclockwise;

  private MapLocation wallPoint;

  private HashSet<MapLocation> visited;

  public BugNav(RobotController rc) {
    this.rc = rc;
    this.isBugging = false;
    this.nextBugCounterclockwise = true;
    this.visited = new HashSet<MapLocation>();

    // The initial last direction is arbitrary, but shouldn't matter.
    this.lastDirection = Direction.NORTH;
  }

  public void setGoal(MapLocation goal) {
    // TODO(kxing): Make this logic more watertight.
    if (!goal.equals(this.goal)) {
      nextBugCounterclockwise = true;
      visited.clear();
    }
    this.goal = goal;
    isBugging = false;
  }

  private boolean tryMove(Direction d, DirectionEvaluations directionEvaluation) throws GameActionException {
    if (!rc.canMove(d)) {
      return false;
    }
    if (directionEvaluation.isDangerous(d)) {
      return false;
    }
    rc.move(d);
    lastDirection = d;
    return true;
  }

  public boolean moveTowardsGoal(DirectionEvaluations directionEvaluations) throws GameActionException {
    if (!rc.isActive()) {
      return false;
    }
    if (rc.getLocation().equals(goal)) {
      return true;
    }
    MapLocation current = rc.getLocation();
    Direction goalDirection = current.directionTo(goal);

    if (!isBugging) {
      if (tryMove(goalDirection, directionEvaluations)) {
        return true;
      }
      if (tryMove(goalDirection.rotateLeft(), directionEvaluations)) {
        return true;
      }
      if (tryMove(goalDirection.rotateRight(), directionEvaluations)) {
        return true;
      }
      isBugging = true;
      wallPoint = current.add(goalDirection);

      Direction counterclockwiseDirection = goalDirection;
      do {
        counterclockwiseDirection = counterclockwiseDirection.rotateRight();
      } while ((!rc.canMove(counterclockwiseDirection) || directionEvaluations.isDangerous(counterclockwiseDirection)) &&
               counterclockwiseDirection != goalDirection);
      
      Direction clockwiseDirection = goalDirection;
      do {
        clockwiseDirection = clockwiseDirection.rotateLeft();
      } while ((!rc.canMove(clockwiseDirection) || directionEvaluations.isDangerous(clockwiseDirection)) &&
               clockwiseDirection != goalDirection);

      if (current.add(counterclockwiseDirection).distanceSquaredTo(goal) <=
          current.add(clockwiseDirection).distanceSquaredTo(goal)) {
        nextBugCounterclockwise = true;
      } else {
        nextBugCounterclockwise = false;
      }

      minimumDistanceWhileBugging = current.distanceSquaredTo(goal);
    }

    // See if we can stop bugging.
    if (!visited.contains(current)
            && !Utils.areDirectionsWithin45(goalDirection, lastDirection.opposite())
            && current.add(goalDirection).distanceSquaredTo(goal) < minimumDistanceWhileBugging
            && tryMove(goalDirection, directionEvaluations)) {
      isBugging = false;
      visited.add(current);
      return true;
    }

    if (minimumDistanceWhileBugging > current.distanceSquaredTo(goal)) {
      minimumDistanceWhileBugging = current.distanceSquaredTo(goal);
    }

    // Follow a "wall".
    Direction directionToWall = current.directionTo(wallPoint);

    if (tryMove(directionToWall, directionEvaluations)) {
      // In case the "wall" was a bot that moved out of the way.
      MapLocation newLocation = current.add(directionToWall);
      Direction newDirectionToGoal = newLocation.directionTo(goal);
      wallPoint = newLocation.add(newDirectionToGoal);
      return true;
    }

    Direction testDirection;
    
    if (nextBugCounterclockwise) {
      testDirection = directionToWall.rotateRight();
    } else {
      testDirection = directionToWall.rotateLeft();
    }

    do {
      if (tryMove(testDirection, directionEvaluations)) {
        if (rc.senseTerrainTile(wallPoint) == TerrainTile.OFF_MAP ||
            rc.senseTerrainTile(wallPoint.add(directionToWall)) == TerrainTile.OFF_MAP) {
          if (Utils.areDirectionsWithin45(goalDirection, lastDirection.opposite())) {
            nextBugCounterclockwise = !nextBugCounterclockwise;
          }
        }
        return true;
      }
      wallPoint = current.add(testDirection);
      if (nextBugCounterclockwise) {
        testDirection = testDirection.rotateRight();
      } else {
        testDirection = testDirection.rotateLeft();
      }
    } while (testDirection != directionToWall);

    return false;
  }
}
