package seeding;

import battlecode.common.*;
import seeding.bots.*;

/**
 * Main entry point.
 * 
 * @author kxing
 */
public class RobotPlayer {
	
	public static void run(RobotController rc) {
    while (true) {
      try {
        switch(rc.getType()) {
          case SOLDIER:
            (new SoldierBot(rc)).run();
            break;
          case HQ:
            (new HeadquartersBot(rc)).run();
            break;
          case PASTR:
            (new PastrBot(rc)).run();
            break;
          case NOISETOWER:
            (new NoisetowerBot(rc)).run();
            break;
          default:
            System.err.println(String.format("Invalid type detected: %s",
                    rc.getType().toString()));
        }
      } catch (Exception e) {
        e.printStackTrace();
      }
    }
	}
}
