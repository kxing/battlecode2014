package seeding.bots;

import battlecode.common.*;
import seeding.communication.*;
import seeding.nav.*;
import seeding.sensing.*;
import seeding.utils.*;

/**
 * Code for soldier-specific behavior.
 *
 * @author kxing
 */
public class SoldierBot extends Bot {
  private BugNav bugNav;

  private MapLocation MY_LOCATION;
  private final int ID;

  private static final double SELF_ATTACK_BONUS = 15.0;
  private static final double NEAR_ENEMY_PASTR_BONUS = 5.0;
  private static final double ENEMY_SOLDIER_RANGE_PENALTY = -10.0;
  private static final double OCCUPIED_ENEMY_SOLDIER_RANGE_BONUS = 7.0;
  private static final double ENEMY_TOWER_RANGE_PENALTY = -50.0;
  private static final double ENEMY_TOWER_SPLASH_PENALTY = -25.0;
  private static final double ILLEGAL_MOVE = -1.0e9;

  // Happens to be the suicide base damage.
  private static final double LOW_HEALTH = 40.0;
  private static final double LOTS_OF_HEALTH = 90.0;

  private enum SoldierType {
    RUSHER,
    PASTR_BUILDER_OR_DEFENDER,
    NOISETOWER_BUILDER,
  };

  private SoldierType type;

  private boolean isTeamFarming;
  private MapLocation idealPastrLocation;
  private MapLocation idealNoisetowerLocation;
  
  private MapLocation currentGoal;
  private DirectionEvaluations directionEvaluations;
  private SensorInfo sensorInfo;

  private boolean intendToSelfDestruct;

  private static final double ACTION_DELAY_UNSAFE = 0.5;
  private static final double ACTION_DEALY_REDUCTION_BONUS = 0.5;

  private static final double ENEMY_COWS_THRESHOLD = 50.0;

  public static final int FARMING_QUOTA = 8;

  private static final int ENEMY_PASTR_CLOSE_DISTANCE = 41;
  private static final int ENEMY_TARGET_CLOSE_DISTANCE = 34;

  private static final int TWO_SQUARES_AWAY = 8;

  public SoldierBot(RobotController rc) {
    super(rc);
    this.bugNav = new BugNav(rc);

    this.ID = rc.getRobot().getID();

    this.currentGoal = rc.senseEnemyHQLocation();
    this.directionEvaluations = new DirectionEvaluations();
    this.bugNav.setGoal(currentGoal);
    this.sensorInfo = new SensorInfo(rc);

    this.type = SoldierType.RUSHER;
    this.intendToSelfDestruct = false;

    this.isTeamFarming = false;
  }

  public void performRoundActions() throws GameActionException {
    MY_LOCATION = rc.getLocation();

    if (intendToSelfDestruct) {
      considerSelfDestruct();
    }

    if (!rc.isActive()) {
      considerSelfDestruct();
      return;
    }
    intendToSelfDestruct = false;
    
    sensorInfo.clear();
    directionEvaluations.clear();

    dissuadeFromEnemyHeadquarters();

    // Consider making a move to self-destruct.
    if (considerSelfDestructMove()) {
      bugNav.setGoal(currentGoal);
      return;
    }

    // Stop if we have made an attacking move.
    if (attackingMove()) {
      bugNav.setGoal(currentGoal);
      return;
    }
    
    handleBroadcasts();
    adjustSoldierType();

    MapLocation newGoal = getGoal();
    if (!currentGoal.equals(newGoal)) {
      currentGoal = newGoal;
      bugNav.setGoal(newGoal);
    }

    switch (type) {
      case RUSHER:
        if (runRusherCode()) {
          return;
        }
        break;
      case PASTR_BUILDER_OR_DEFENDER:
        if (runPastrBuilderOrDefenderCode()) {
          return;
        }
        break;
      case NOISETOWER_BUILDER:
        if (runNoisetowerBuilderCode()) {
          return;
        }
        break;
      default:
        assert(false);
    }

    bugNav.moveTowardsGoal(directionEvaluations);
  }

  private void dissuadeFromEnemyHeadquarters() throws GameActionException {
    for (int i = 0; i < DirectionEvaluations.DIRECTIONS.length; i++) {
      Direction d = DirectionEvaluations.DIRECTIONS[i];
      MapLocation candidateSquare = MY_LOCATION.add(d);

      if (ENEMY_TEAM_HEADQUARTERS.distanceSquaredTo(candidateSquare) <= RobotType.HQ.attackRadiusMaxSquared) {
        // Don't stand within headquarters range.
        directionEvaluations.addToDirection(
                ENEMY_TOWER_RANGE_PENALTY + ENEMY_TOWER_RANGE_PENALTY /
                (ENEMY_TEAM_HEADQUARTERS.distanceSquaredTo(candidateSquare) + 1), i);
      } else {
        Direction splashDirection = ENEMY_TEAM_HEADQUARTERS.directionTo(candidateSquare);
        if (ENEMY_TEAM_HEADQUARTERS.add(splashDirection).distanceSquaredTo(candidateSquare) <= RobotType.HQ.attackRadiusMaxSquared) {
          // Don't stand within splash range.
          directionEvaluations.addToDirection(ENEMY_TOWER_SPLASH_PENALTY, i);
        }
      }
    }
  }

  // Returns we should stop.
  private boolean attackingMove() throws GameActionException {
    sensorInfo.senseEnemyRobots(true);

    if (sensorInfo.getEnemyRobots().length == 0) {
      return false;
    }

    sensorInfo.senseMyRobots(true);

    // Precompute the dangerous enemy soldiers and also whether we can shoot something.

    boolean canShoot = false;
    int dangerousSoldiersThatCanShootMe = 0;

    RobotInfo[] enemyInfos = sensorInfo.getEnemyInfos();
    RobotInfo[] myInfos = sensorInfo.getMyInfos();

    boolean[] isSoldierDangerous = new boolean[enemyInfos.length];
    for (int i = 0; i < isSoldierDangerous.length; i++) {
      if (MY_LOCATION.distanceSquaredTo(enemyInfos[i].location) <= ATTACK_RADIUS_SQUARED) {
        canShoot = true;
      }
      if (enemyInfos[i].type != RobotType.SOLDIER) {
        continue;
      }
      MapLocation ml = enemyInfos[i].location;

      isSoldierDangerous[i] = true;
      for (RobotInfo ri : myInfos) {
        if (ml.distanceSquaredTo(ri.location) <= RobotType.SOLDIER.attackRadiusMaxSquared) {
          isSoldierDangerous[i] = false;
          break;
        }
      }

      if (ml.distanceSquaredTo(MY_LOCATION) <= RobotType.SOLDIER.attackRadiusMaxSquared) {
        dangerousSoldiersThatCanShootMe++;
      }
    }

    if (!directionEvaluations.isDangerous(DirectionEvaluations.ATTACK_SENTINEL) &&
        canShoot && dangerousSoldiersThatCanShootMe <= 1) {
      attack();
      return true;
    }

    boolean[] isTargetRelevant = new boolean[enemyInfos.length];
    for (int i = 0; i < isTargetRelevant.length; i++) {
      RobotInfo ri = enemyInfos[i];
      if (ri.type != RobotType.PASTR && ri.type != RobotType.NOISETOWER) {
        continue;
      }
      isTargetRelevant[i] = unhinderedPathTo(MY_LOCATION, ri.location);
    }

    for (int i = 0; i < DirectionEvaluations.DIRECTIONS.length; i++) {
      Direction d = DirectionEvaluations.DIRECTIONS[i];
      MapLocation candidateSquare = MY_LOCATION.add(d);

      if (d != Direction.NONE && !rc.canMove(d)) {
        directionEvaluations.addToDirection(ILLEGAL_MOVE, i);
        continue;
      }
      
      for (int j = 0; j < enemyInfos.length; j++) {
        RobotInfo ri = enemyInfos[j];
        
        switch (ri.type) {
          case HQ:
            // Already accounted for in dissuadeFromEnemyHeadquarters().
            break;
          case SOLDIER:
            if (!isSoldierDangerous[j]) {
              // Move towards an enemy soldier that's in range of an ally.
              directionEvaluations.addToDirection(OCCUPIED_ENEMY_SOLDIER_RANGE_BONUS / (candidateSquare.distanceSquaredTo(ri.location) + 1), i);
            } else if (enemyInfos.length * 2 < myInfos.length) {
              // Don't care - the opponent is greatly outnumbered.
            } else if (ri.location.distanceSquaredTo(candidateSquare) <= RobotType.SOLDIER.attackRadiusMaxSquared) {
              // Don't engage if we don't have an advantage.
              directionEvaluations.addToDirection(ENEMY_SOLDIER_RANGE_PENALTY, i);
            }
            break;
          case PASTR:
          case NOISETOWER:
            if (isTargetRelevant[j] && type == SoldierType.RUSHER) {
              // Get closer to a pastr or noisetower, if we can go there directly.
              directionEvaluations.addToDirection(
                      NEAR_ENEMY_PASTR_BONUS / (candidateSquare.distanceSquaredTo(ri.location) + 1), i);
            }
            break;
          default:
            // Don't care.
            break;
        }
      }
    }

    // Consider attacking.
    for (RobotInfo ri : sensorInfo.getEnemyInfos()) {
      if (ri.type != RobotType.HQ &&
          ri.location.distanceSquaredTo(MY_LOCATION) <= ATTACK_RADIUS_SQUARED) {
        directionEvaluations.addToDirection(SELF_ATTACK_BONUS, DirectionEvaluations.ATTACK_INDEX);
        break;
      }
    }

    if (rc.getActionDelay() >= ACTION_DELAY_UNSAFE) {
      // Favor doing nothing if our action delay is getting dangerously high.
      directionEvaluations.addToDirection(ACTION_DEALY_REDUCTION_BONUS,
                                          DirectionEvaluations.NONE_INDEX);
    }

    Direction bestMovement = directionEvaluations.getBestDirection();

    if (directionEvaluations.isMoveForced()) {
      if (bestMovement == DirectionEvaluations.ATTACK_SENTINEL) {
        attack();
      } else {
        rc.move(bestMovement);
      }
      return true;
    }
    return false;
  }

  private MapLocation getGoal() throws GameActionException {
    if (type == SoldierType.PASTR_BUILDER_OR_DEFENDER && idealPastrLocation != null) {
      return idealPastrLocation;
    }

    if (type == SoldierType.NOISETOWER_BUILDER) {
      return idealNoisetowerLocation;
    }

    // Go for the pastr furthest away from the enemy headquarters, or
    // surround the enemy headquarters, if there is no target.
    MapLocation[] enemyPastrs = rc.sensePastrLocations(ENEMY_TEAM);

    MapLocation bestTarget = ENEMY_TEAM_HEADQUARTERS;
    int bestDistance = 0;
    for (int i = 0; i < enemyPastrs.length; i++) {
      int distanceSquared =
              enemyPastrs[i].distanceSquaredTo(ENEMY_TEAM_HEADQUARTERS);
      if (bestDistance < distanceSquared) {
        bestDistance = distanceSquared;
        bestTarget = enemyPastrs[i];
      }
    }

    return bestTarget;
  }

  /**
   * Returns whether there is a clear path between two locations (ie. not
   * separated by voids).
   *
   * Note this method is pretty expensive, so don't call this multiple times!
   *
   * @param start The starting location.
   * @param target The target location.
   * @return Whether the locations are not separated by voids.
   */
  private boolean unhinderedPathTo(MapLocation start, MapLocation target) throws GameActionException {
    MapLocation current = start;
    while (!current.equals(target)) {
      Direction d = current.directionTo(target);
      current = current.add(d);
      if (!Utils.isTraversable(rc, current) || rc.senseObjectAtLocation(current) != null) {
        return false;
      }
    }
    return true;
  }

  private void attack() throws GameActionException {
    MapLocation bestTarget = null;
    RobotType bestTargetType = null;
    double bestTargetHp = Double.MAX_VALUE;
    
    for (RobotInfo ri : sensorInfo.getEnemyInfos()) {
      MapLocation potentialTarget = ri.location;
      if (MY_LOCATION.distanceSquaredTo(potentialTarget) > ATTACK_RADIUS_SQUARED) {
        // Out of range.
        continue;
      }

      boolean isBetterTarget = false;
      // soldier > pastr > noisetower.
      switch (ri.type) {
        case SOLDIER:
          // Prioritize soldiers, especially ones with the least amount of health.
          if (bestTargetType != RobotType.SOLDIER || ri.health < bestTargetHp) {
            isBetterTarget = true;
          }
          break;
        case PASTR:
          // Prioritize pastrs after soldiers.
          if (bestTargetType != RobotType.SOLDIER &&
              (bestTargetType != RobotType.PASTR || ri.health < bestTargetHp)) {
            isBetterTarget = true;
          }
          break;
        case NOISETOWER:
          if (bestTargetType != RobotType.SOLDIER &&
              bestTargetType != RobotType.PASTR &&
              (bestTargetType != RobotType.NOISETOWER || ri.health < bestTargetHp)) {
            isBetterTarget = true;
          }
          break;
        default:
          break;
      }

      if (isBetterTarget) {
        bestTarget = potentialTarget;
        bestTargetType = ri.type;
        bestTargetHp = ri.health;
      }
    }

    if (bestTarget == null && type == SoldierType.RUSHER) {
      // Attack a square with cows that is under the enemy pastr.
      if (!currentGoal.equals(ENEMY_TEAM_HEADQUARTERS) &&
          MY_LOCATION.distanceSquaredTo(currentGoal) <= ENEMY_PASTR_CLOSE_DISTANCE) {
        Direction d = currentGoal.directionTo(MY_LOCATION);
        MapLocation[] potentialSquares = new MapLocation[] {
          currentGoal.add(d),
          currentGoal.add(d).add(d),
          currentGoal.add(d).add(d.rotateLeft()),
          currentGoal.add(d).add(d.rotateRight()),
          currentGoal.add(d).add(d).add(d),
          currentGoal.add(d).add(d).add(d.rotateLeft()),
          currentGoal.add(d).add(d).add(d.rotateRight()),
        };

        double mostCows = ENEMY_COWS_THRESHOLD;
        for (MapLocation ml : potentialSquares) {
          if (!rc.canAttackSquare(ml) ||
              rc.senseObjectAtLocation(ml) != null) {
            // Out of range or something in the square.
            continue;
          }
          double cows = rc.senseCowsAtLocation(ml);
          if (mostCows < cows) {
            bestTarget = ml;
            mostCows = cows;
          }
        }
      }
    }

    if (bestTarget != null) {
      rc.attackSquare(bestTarget);
    }
  }

  public boolean performLongComputation() throws GameActionException {
    return false;
  }

  private void handleBroadcasts() throws GameActionException {
    if (idealPastrLocation == null && communication.receiveComputationsDone()) {
      // Handle pastr building.
      idealPastrLocation = communication.receivePastrLocation();
    }

    if (idealNoisetowerLocation == null && communication.receiveComputationsDone()) {
      // Handle noisetower building.
      idealNoisetowerLocation = communication.receiveNoisetowerLocation();
    }

    if (!isTeamFarming && communication.receiveFarmSignal()) {
      isTeamFarming = true;
    }
  }

  private void considerSelfDestruct() throws GameActionException {
    double totalDamage = -rc.getHealth();
    double splashDamage = GameConstants.SELF_DESTRUCT_BASE_DAMAGE +
            GameConstants.SELF_DESTRUCT_DAMAGE_FACTOR * rc.getHealth();

    for (Direction d : Utils.MOVEMENT_DIRECTIONS) {
      GameObject go = rc.senseObjectAtLocation(MY_LOCATION.add(d));
      if (go == null || !(go instanceof Robot)) {
        continue;
      }

      RobotInfo ri = rc.senseRobotInfo((Robot)(go));
      double damage = Math.min(ri.health, splashDamage);
      if (ri.team == MY_TEAM) {
        totalDamage -= damage;
      } else if (ri.team == ENEMY_TEAM) {
        totalDamage += damage;
      }
    }

    if (totalDamage > 0.0) {
      rc.selfDestruct();
      rc.yield();
    }
  }

  private boolean considerSelfDestructMove() throws GameActionException {
    sensorInfo.senseEnemyRobots(true);

    if (sensorInfo.getEnemyRobots().length == 0) {
      return false;
    }

    sensorInfo.senseMyRobots(true);

    double[] totalDamage = new double[Utils.MOVEMENT_DIRECTIONS.length];

    for (RobotInfo ri : sensorInfo.getEnemyInfos()) {
      if (MY_LOCATION.distanceSquaredTo(ri.location) > TWO_SQUARES_AWAY) {
        continue;
      }
      for (int i = 0; i < Utils.MOVEMENT_DIRECTIONS.length; i++) {
        MapLocation newLocation = MY_LOCATION.add(Utils.MOVEMENT_DIRECTIONS[i]);
        if (newLocation.isAdjacentTo(ri.location)) {
          totalDamage[i] += Math.min(ri.health, GameConstants.SELF_DESTRUCT_BASE_DAMAGE);
        }
      }
    }

    for (RobotInfo ri : sensorInfo.getMyInfos()) {
      if (MY_LOCATION.distanceSquaredTo(ri.location) > TWO_SQUARES_AWAY) {
        continue;
      }
      for (int i = 0; i < Utils.MOVEMENT_DIRECTIONS.length; i++) {
        MapLocation newLocation = MY_LOCATION.add(Utils.MOVEMENT_DIRECTIONS[i]);
        if (newLocation.isAdjacentTo(ri.location)) {
          totalDamage[i] -= Math.min(ri.health, GameConstants.SELF_DESTRUCT_BASE_DAMAGE);
        }
      }
    }

    double bestDamageTaken = Double.MAX_VALUE;
    Direction bestDirection = null;

    for (int i = 0; i < Utils.MOVEMENT_DIRECTIONS.length; i++) {
      if (totalDamage[i] <= rc.getHealth()) {
        // Not worth self-destructing.
        continue;
      }

      Direction direction = Utils.MOVEMENT_DIRECTIONS[i];
      if (!rc.canMove(direction)) {
        continue;
      }

      if (directionEvaluations.isDangerous(direction)) {
        // Avoid taking headquarters damage.
        continue;
      }

      double damageTaken = 0.0;

      MapLocation newLocation = MY_LOCATION.add(direction);
      for (RobotInfo ri : sensorInfo.getEnemyInfos()) {
        if (ri.type != RobotType.SOLDIER) {
          // Pastrs and noisetowers can't attack us.
          continue;
        }
        if (ri.location.distanceSquaredTo(newLocation) > RobotType.SOLDIER.attackRadiusMaxSquared) {
          // Too far away to attack us.
          continue;
        }
        if (ri.actionDelay >= 2.0 || (ri.robot.getID() < ID && ri.actionDelay >= 1.0)) {
          // Can't actually attack us.
          continue;
        }

        damageTaken += RobotType.SOLDIER.attackPower;
      }

      if (bestDamageTaken > damageTaken) {
        bestDamageTaken = damageTaken;
        bestDirection = direction;
      }
    }

    if (bestDirection != null && bestDamageTaken < rc.getHealth()) {
      rc.move(bestDirection);
      intendToSelfDestruct = true;
      return true;
    }
    
    return false;
  }

  private boolean isNoisetowerBuilt() throws GameActionException {
    if (idealNoisetowerLocation == null) {
      return false;
    }
    if (!rc.canSenseSquare(idealNoisetowerLocation)) {
      return false;
    }
    
    GameObject go = rc.senseObjectAtLocation(idealNoisetowerLocation);
    if (go != null && go instanceof Robot) {
      RobotInfo ri = rc.senseRobotInfo((Robot)(go));
      if (ri.type == RobotType.NOISETOWER && ri.team == MY_TEAM) {
        return true;
      }
    }

    return false;
  }

  private int getPastrDefenderIndex() throws GameActionException {
    int pastrDefendersClaimed = communication.receivePastrDefendersClaimed();
    int index = -1;
    for (int i = 0; i < pastrDefendersClaimed; i++) {
      if (communication.receivePastrDefenderId(i) == ID) {
        index = i;
        break;
      }
    }
    assert(index != -1);
    return index;
  }

  private void adjustSoldierType() throws GameActionException {
    // Handling the pastr defenders.
    int pastrDefendersNecessary = communication.receivePastrDefendersNecessary();
    int pastrDefendersClaimed = communication.receivePastrDefendersClaimed();
    
    switch (type) {
      case RUSHER:
        // Rusher -> noisetower builder.
        if (isTeamFarming && communication.receiveNoisetowerBuilderId() == Communication.NOBODY &&
            rc.senseNearbyGameObjects(Robot.class, idealNoisetowerLocation, 0, MY_TEAM).length == 0) {
          communication.sendNoisetowerBuilderId(ID);
          type = SoldierType.NOISETOWER_BUILDER;
          return;
        }

        // Rusher -> pastr defender.
        if (rc.getHealth() <= LOW_HEALTH || pastrDefendersClaimed < pastrDefendersNecessary) {
          // Rusher -> Pastr defender.
          communication.sendPastrDefenderId(pastrDefendersClaimed, ID);
          communication.sendPastrDefendersClaimed(pastrDefendersClaimed + 1);
          type = SoldierType.PASTR_BUILDER_OR_DEFENDER;
          return;
        }
        break;
      case PASTR_BUILDER_OR_DEFENDER:
        if (pastrDefendersClaimed > pastrDefendersNecessary &&
            rc.getHealth() >= LOTS_OF_HEALTH) {
          // Pastr defender -> Rusher.
          int index = getPastrDefenderIndex();
          int lastId = communication.receivePastrDefenderId(pastrDefendersClaimed - 1);

          communication.sendPastrDefenderId(index, lastId);
          communication.sendPastrDefendersClaimed(pastrDefendersClaimed - 1);
          type = SoldierType.RUSHER;
          return;
        }
        break;
      default:
        break;
    }
  }

  private boolean runRusherCode() throws GameActionException {
    // Anti-headquarters turtle code.
    if (currentGoal.distanceSquaredTo(ENEMY_TEAM_HEADQUARTERS) <= GameConstants.PASTR_RANGE &&
        MY_LOCATION.distanceSquaredTo(ENEMY_TEAM_HEADQUARTERS) <= ENEMY_TARGET_CLOSE_DISTANCE &&
        !directionEvaluations.isDangerous(DirectionEvaluations.ATTACK_SENTINEL)) {
      // Consider attacking cows.
      attack();
      return true;
    }

    return false;
  }

  private boolean runPastrBuilderOrDefenderCode() throws GameActionException {
    if (isTeamFarming && MY_LOCATION.equals(idealPastrLocation) &&
        isNoisetowerBuilt()) {
      sensorInfo.senseMyRobots(false);
      if (sensorInfo.getMyRobots().length >= communication.receivePastrDefendersNecessary()) {
        rc.construct(RobotType.PASTR);
        return true;
      }
    }

    if (MY_LOCATION.isAdjacentTo(idealPastrLocation) &&
        !MY_LOCATION.equals(idealNoisetowerLocation) &&
        !directionEvaluations.isDangerous(DirectionEvaluations.ATTACK_SENTINEL)) {
      GameObject go = rc.senseObjectAtLocation(idealPastrLocation);
      if (go != null && go.getTeam() == MY_TEAM) {
        // Do nothing, a pastr will be built -> stay put.
        return true;
      }
    }
    return false;
  }

  private boolean runNoisetowerBuilderCode() throws GameActionException {
    if (isTeamFarming && MY_LOCATION.equals(idealNoisetowerLocation)) {
      rc.construct(RobotType.NOISETOWER);
      return true;
    }
    return false;
  }
}
