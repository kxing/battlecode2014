package seeding.bots;

import battlecode.common.*;
import seeding.communication.*;
import seeding.sensing.*;
import seeding.utils.Utils;

/**
 * Code for headquarters-specific behavior.
 *
 * @author kxing
 */
public class HeadquartersBot extends Bot {

  private final MapLocation MY_LOCATION;
  private final double[][] COW_GROWTHS;
  private final int MAP_WIDTH;
  private final int MAP_HEIGHT;

  private final MapLocation[] spawnSquares;
  private final SensorInfo sensorInfo;

  private boolean computedFarmingLocations;
  private MapLocation idealPastrLocation;
  private MapLocation idealNoisetowerLocation;

  private static final double EPSILON = 1.0e-4;
  private static final double FRIENDLY_FIRE_PENALTY = 25.0;
  
  private static final int LATE_GAME = 1000;
  private static final double WORRISOME_AMOUNT = 0.2;

  public HeadquartersBot(RobotController rc) {
    super(rc);
    this.MY_LOCATION = rc.getLocation();
    this.COW_GROWTHS = rc.senseCowGrowth();
    this.MAP_WIDTH = rc.getMapWidth();
    this.MAP_HEIGHT = rc.getMapHeight();

    this.spawnSquares = computeSpawnSquares();
    this.sensorInfo = new SensorInfo(rc);

    this.computedFarmingLocations = false;
  }

  public void performRoundActions() throws GameActionException {
    sensorInfo.clear();
    doTeamCommunication();
//    rc.setIndicatorString(0, "There are " + getNumberOfSoldiers() + " friendly soldiers.");

    // Spawn if we can.
    if (!rc.isActive()) {
      return;
    }

    if (attack()) {
      return;
    }
    spawn();
  }

  private MapLocation[] computeSpawnSquares() {
    MapLocation[] availableSquares = new MapLocation[Utils.MOVEMENT_DIRECTIONS.length];
    int numberOfAvailableSquares = 0;
    
    Direction toEnemy = MY_TEAM_HEADQUARTERS.directionTo(ENEMY_TEAM_HEADQUARTERS);
    Direction[] directions = new Direction[] {
      toEnemy,
      toEnemy.rotateRight(),
      toEnemy.rotateLeft(),
      toEnemy.rotateRight().rotateRight(),
      toEnemy.rotateLeft().rotateLeft(),
      toEnemy.opposite().rotateLeft(),
      toEnemy.opposite().rotateRight(),
      toEnemy.opposite(),
    };

    for (Direction d : directions) {
      MapLocation candidateSquare = MY_TEAM_HEADQUARTERS.add(d);
      if (Utils.isTraversable(rc, candidateSquare)) {
        availableSquares[numberOfAvailableSquares++] = candidateSquare;
      }
    }

    MapLocation[] toReturn = new MapLocation[numberOfAvailableSquares];
    System.arraycopy(availableSquares, 0, toReturn, 0, numberOfAvailableSquares);
    return toReturn;
  }

  // Returns whether we actually attacked.
  private boolean attack() throws GameActionException {
    sensorInfo.senseEnemyRobots(true);
    sensorInfo.senseMyRobots(true);

    MapLocation bestAttackSquare = null;
    double bestAttackDamage = 0.0;

    for (RobotInfo ri : sensorInfo.getEnemyInfos()) {
      if (ri.location.distanceSquaredTo(MY_LOCATION) > ATTACK_RADIUS_SQUARED) {
        continue;
      }

      double attackDamage = computeAttackDamage(ri.location);
      if (bestAttackDamage + EPSILON < attackDamage) {
        bestAttackDamage = attackDamage;
        bestAttackSquare = ri.location;
      }
    }

    for (RobotInfo ri : sensorInfo.getEnemyInfos()) {
      MapLocation enemyLocation = ri.location;
      Direction d = enemyLocation.directionTo(MY_LOCATION);
      MapLocation candidateTarget = enemyLocation.add(d);
      if (candidateTarget.distanceSquaredTo(MY_LOCATION) > ATTACK_RADIUS_SQUARED) {
        continue;
      }

      double attackDamage = computeAttackDamage(candidateTarget);
      if (bestAttackDamage + EPSILON < attackDamage) {
        bestAttackDamage = attackDamage;
        bestAttackSquare = candidateTarget;
      }
    }

    if (bestAttackSquare != null) {
      rc.attackSquare(bestAttackSquare);
      return true;
    }

    return false;
  }

  private double computeAttackDamage(MapLocation ml) throws GameActionException {
    double attackDamage = 0.0;
    for (RobotInfo ri : sensorInfo.getEnemyInfos()) {
      if (ri.type == RobotType.HQ) {
        // The headquarters takes no damage.
        continue;
      }
      
      if (ri.location.equals(ml)) {
        attackDamage += RobotType.HQ.attackPower;
      } else if (ri.location.isAdjacentTo(ml)) {
        attackDamage += RobotType.HQ.splashPower;
      }
    }
    for (RobotInfo ri : sensorInfo.getMyInfos()) {
      if (ri.type == RobotType.HQ) {
        // The headquarters takes no damage.
        continue;
      }
      
      if (ri.location.equals(ml)) {
        attackDamage -= (RobotType.HQ.attackPower + FRIENDLY_FIRE_PENALTY);
      } else if (ri.location.isAdjacentTo(ml)) {
        attackDamage -= (RobotType.HQ.splashPower + FRIENDLY_FIRE_PENALTY);
      }
    }
    return attackDamage;
  }

  private boolean spawn() throws GameActionException {
    if (rc.senseRobotCount() < GameConstants.MAX_ROBOTS) {
      for (MapLocation ml : spawnSquares) {
        if (rc.senseObjectAtLocation(ml) == null) {
          rc.spawn(MY_TEAM_HEADQUARTERS.directionTo(ml));
          return true;
        }
      }
    }
    return false;
  }

  private int getNumberOfSoldiers() throws GameActionException {
    int count = 0;
    Robot[] myRobots = rc.senseNearbyGameObjects(Robot.class, MAP_WIDTH * MAP_WIDTH + MAP_HEIGHT * MAP_HEIGHT, MY_TEAM);
    for (Robot r : myRobots) {
      RobotInfo ri = rc.senseRobotInfo(r);
      if (ri.type == RobotType.SOLDIER) {
        count++;
      }
    }
    return count;
  }

  private static final int GRAIN_SIZE = 7;
  private int grainXLength;
  private int grainYLength;
  private double[][] grainWeights;

  private class ComputationState {
    double bestWeight;
    int bestX;
    int bestY;
    int bestDistance;
    int grainX;
    int grainY;
  }

  private ComputationState computationState = null;

  public boolean performLongComputation() throws GameActionException {
    if (computedFarmingLocations) {
      return false;
    }

    if (computationState == null) {
      grainXLength = (MAP_WIDTH + GRAIN_SIZE - 1) / GRAIN_SIZE;
      grainYLength = (MAP_HEIGHT + GRAIN_SIZE - 1) / GRAIN_SIZE;

      grainWeights = new double[grainXLength][grainYLength];

      computationState = new ComputationState();
      computationState.bestWeight = 0.0;
      computationState.bestX = 0;
      computationState.bestY = 0;
      computationState.bestDistance = Integer.MAX_VALUE;
      computationState.grainX = 0;
      computationState.grainY = 0;
    }

    int grainX = computationState.grainX;
    int grainY = computationState.grainY;

    if (grainX * GRAIN_SIZE + GRAIN_SIZE < MAP_WIDTH) {
      for (int x = GRAIN_SIZE * grainX; x < GRAIN_SIZE * grainX + GRAIN_SIZE; x++) {
        for (int y = GRAIN_SIZE * grainY; y < GRAIN_SIZE * grainY + GRAIN_SIZE; y++) {
          if (Utils.isTraversable(rc, new MapLocation(x, y))) {
            grainWeights[grainX][grainY] += COW_GROWTHS[x][y];
          }
        }
      }

      boolean isBetter = false;
      if (computationState.bestWeight < grainWeights[grainX][grainY]) {
        isBetter = true;
      } else if (computationState.bestWeight == grainWeights[grainX][grainY] &&
                 computationState.bestDistance > getGrainCenter(grainX, grainY).distanceSquaredTo(MY_TEAM_HEADQUARTERS)) {
        isBetter = true;
      }

      if (isBetter) {
        computationState.bestWeight = grainWeights[grainX][grainY];
        computationState.bestX = grainX;
        computationState.bestY = grainY;
        computationState.bestDistance = getGrainCenter(grainX, grainY).distanceSquaredTo(MY_TEAM_HEADQUARTERS);
      }

      grainY++;
      if (grainY * GRAIN_SIZE + GRAIN_SIZE >= MAP_HEIGHT) {
        grainY = 0;

        grainX++;
      }
      computationState.grainX = grainX;
      computationState.grainY = grainY;
      return true;
    } else {
      idealPastrLocation = findNearbyTraversableSquare(getGrainCenter(computationState.bestX, computationState.bestY), null);
      idealNoisetowerLocation = findNearbyTraversableSquare(idealPastrLocation, idealPastrLocation);


      communication.sendPastrLocation(idealPastrLocation);
      communication.sendNoisetowerLocation(idealNoisetowerLocation);
      communication.sendComputationsDone(true);
      computedFarmingLocations = true;
      return false;
    }
  }

  private static MapLocation getGrainCenter(int grainX, int grainY) throws GameActionException {
    return new MapLocation(grainX * GRAIN_SIZE + GRAIN_SIZE / 2, grainY * GRAIN_SIZE + GRAIN_SIZE / 2);
  }

  private MapLocation findNearbyTraversableSquare(MapLocation center, MapLocation squareToAvoid) throws GameActionException {
    MapLocation testLocation;
    for (int offset = 0; ; offset++) {
      for (int x = center.x - offset; x <= center.x + offset; x++) {
        testLocation = new MapLocation(x, center.y - offset);
        if (Utils.isTraversable(rc, testLocation) && !testLocation.equals(squareToAvoid)) {
          return testLocation;
        }

        testLocation = new MapLocation(x, center.y - offset);
        if (Utils.isTraversable(rc, testLocation) && !testLocation.equals(squareToAvoid)) {
          return testLocation;
        }
      }

      for (int y = center.y - offset; y <= center.y + offset; y++) {
        testLocation = new MapLocation(center.x - offset, y);
        if (Utils.isTraversable(rc, testLocation) && !testLocation.equals(squareToAvoid)) {
          return testLocation;
        }
        
        testLocation = new MapLocation(center.x + offset, y);
        if (Utils.isTraversable(rc, testLocation) && !testLocation.equals(squareToAvoid)) {
          return testLocation;
        }
      }
    }
  }

  private void doTeamCommunication() throws GameActionException {
    if (computedFarmingLocations && !communication.receiveFarmSignal()) {
      boolean shouldSendSignal = false;
      if (rc.senseNearbyGameObjects(Robot.class, ENEMY_TEAM_HEADQUARTERS, 50, MY_TEAM).length >= SoldierBot.FARMING_QUOTA) {
        shouldSendSignal = true;
      } else if (Clock.getRoundNum() >= LATE_GAME) {
        shouldSendSignal = true;
      } else if (rc.senseTeamMilkQuantity(ENEMY_TEAM) > GameConstants.WIN_QTY * WORRISOME_AMOUNT) {
        shouldSendSignal = true;
      }

      if (shouldSendSignal) {
        sendFarmingSignal();
      }
    }

    Robot[] myRobots = null;

    int noisetowerBuilderId = communication.receiveNoisetowerBuilderId();

    if (noisetowerBuilderId != Communication.NOBODY) {
      if (myRobots == null) {
        myRobots = rc.senseNearbyGameObjects(
                Robot.class,
                GameConstants.MAP_MAX_WIDTH * GameConstants.MAP_MAX_WIDTH + GameConstants.MAP_MAX_HEIGHT * GameConstants.MAP_MAX_HEIGHT,
                MY_TEAM);
      }
      boolean foundNoisetowerBuilder = false;
      for (Robot r : myRobots) {
        if (r.getID() == noisetowerBuilderId) {
          foundNoisetowerBuilder = true;
          break;
        }
      }

      if (!foundNoisetowerBuilder) {
        communication.sendNoisetowerBuilderId(Communication.NOBODY);
      }
    }

    int defendersClaimed = communication.receivePastrDefendersClaimed();
    if (defendersClaimed > 0) {
      if (myRobots == null) {
        myRobots = rc.senseNearbyGameObjects(
                Robot.class,
                GameConstants.MAP_MAX_WIDTH * GameConstants.MAP_MAX_WIDTH + GameConstants.MAP_MAX_HEIGHT * GameConstants.MAP_MAX_HEIGHT,
                MY_TEAM);
      }
      int[] pastrDefenderIds = new int[defendersClaimed];
      for (int i = 0; i < pastrDefenderIds.length; i++) {
        pastrDefenderIds[i] = communication.receivePastrDefenderId(i);
      }

      int oldDefendersClaimed = defendersClaimed;

      for (int i = pastrDefenderIds.length; --i >= 0; ) {
        boolean found = false;
        for (Robot r : myRobots) {
          if (r.getID() == pastrDefenderIds[i]) {
            found = true;
            break;
          }
        }
        if (!found) {
          int temp = pastrDefenderIds[defendersClaimed - 1];
          pastrDefenderIds[defendersClaimed - 1] = pastrDefenderIds[i];
          pastrDefenderIds[i] = temp;

          defendersClaimed--;
        }
      }

      if (defendersClaimed < oldDefendersClaimed) {
        for (int i = 0; i < defendersClaimed; i++) {
          communication.sendPastrDefenderId(i, pastrDefenderIds[i]);
        }
        communication.sendPastrDefendersClaimed(defendersClaimed);
      }
    }
  }
}
