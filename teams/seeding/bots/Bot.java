package seeding.bots;

import battlecode.common.*;
import seeding.communication.*;
import seeding.debugging.*;

/**
 * Generic behavior for all bots.
 *
 * @author kxing
 */
public abstract class Bot {
  
  protected RobotController rc;
  protected Communication communication;

  protected final Team MY_TEAM;
  protected final Team ENEMY_TEAM;
  protected final MapLocation MY_TEAM_HEADQUARTERS;
  protected final MapLocation ENEMY_TEAM_HEADQUARTERS;
  
  protected final int SENSOR_RADIUS_SQUARED;
  protected final int ATTACK_RADIUS_SQUARED;

  protected final BytecodeCounter bytecodeCounter;

  private static final int BYTECODE_BUFFER = 3000;

  public static final int DEFAULT_PASTR_DEFENDERS = 3;

  public Bot(RobotController rc) {
    this.rc = rc;
    this.communication = new Communication(rc);
    
    this.MY_TEAM = rc.getTeam();
    this.ENEMY_TEAM = rc.getTeam().opponent();
    this.MY_TEAM_HEADQUARTERS = rc.senseHQLocation();
    this.ENEMY_TEAM_HEADQUARTERS = rc.senseEnemyHQLocation();
    
    this.SENSOR_RADIUS_SQUARED = rc.getType().sensorRadiusSquared;
    this.ATTACK_RADIUS_SQUARED = rc.getType().attackRadiusMaxSquared;

    this.bytecodeCounter = new BytecodeCounter();
  }
  
  public void run() {
		while(true) {
      bytecodeCounter.start();
      try {
        performRoundActions();

        while (Clock.getBytecodesLeft() > BYTECODE_BUFFER) {
          if (!performLongComputation()) {
            break;
          }
        }
      } catch (Exception e) {
        e.printStackTrace();
      }

      int bytecodesUsed = bytecodeCounter.getBytecodesUsed();
      if (bytecodesUsed >= GameConstants.BYTECODE_LIMIT) {
        System.err.println(String.format("Bytecode limit exceeded: %d", bytecodesUsed));
      }
      bytecodeCounter.stop();
			rc.yield();
		}
  }

  public abstract void performRoundActions() throws GameActionException;

  public abstract boolean performLongComputation() throws GameActionException;

  public void sendFarmingSignal() throws GameActionException {
    communication.sendFarmSignal(true);
    if (communication.receivePastrDefendersNecessary() < DEFAULT_PASTR_DEFENDERS) {
      communication.sendPastrDefendersNecessary(DEFAULT_PASTR_DEFENDERS);
    }
  }
}
