package seeding.communication;

import battlecode.common.*;

/**
 *
 * @author kxing
 */
public class Communication {

  private final RobotController rc;

  private static final int FARM_CHANNEL = 0;
  private static final int COMPUTATIONS_DONE_CHANNEL = 1;
  private static final int PASTR_LOCATION_CHANNEL = 2;
  private static final int NOISETOWER_CHANNEL = 3;

  private static final int NOISETOWER_BUILDER_CHANNEL = 4;

  // Channel 5-7 is unused so far.

  private static final int PASTR_DEFENDERS_NECESSARY_CHANNEL = 8;
  private static final int PASTR_DEFENDERS_CLAIMED_CHANNEL = 9;
  private static final int PASTR_DEFENDERS_OFFSET = 10;

  // Channels 25+ are unused so far.
  
  public static final int NOBODY = 0;

  public Communication(RobotController rc) {
    this.rc = rc;
  }

  public void sendFarmSignal(boolean shouldFarm) throws GameActionException {
    rc.broadcast(FARM_CHANNEL, booleanToInt(shouldFarm));
  }

  public boolean receiveFarmSignal() throws GameActionException {
    int data = rc.readBroadcast(FARM_CHANNEL);
    return intToBoolean(data);
  }

  public void sendComputationsDone(boolean computationsDone) throws GameActionException {
    rc.broadcast(COMPUTATIONS_DONE_CHANNEL, booleanToInt(computationsDone));
  }

  public boolean receiveComputationsDone() throws GameActionException {
    int data = rc.readBroadcast(COMPUTATIONS_DONE_CHANNEL);
    return intToBoolean(data);
  }

  public void sendPastrLocation(MapLocation pastrLocation) throws GameActionException {
    rc.broadcast(PASTR_LOCATION_CHANNEL, mapLocationToInt(pastrLocation));
  }

  public MapLocation receivePastrLocation() throws GameActionException {
    int data = rc.readBroadcast(PASTR_LOCATION_CHANNEL);
    return intToMapLocation(data);
  }

  public void sendNoisetowerLocation(MapLocation noisetowerLocation) throws GameActionException {
    rc.broadcast(NOISETOWER_CHANNEL, mapLocationToInt(noisetowerLocation));
  }

  public MapLocation receiveNoisetowerLocation() throws GameActionException {
    int data = rc.readBroadcast(NOISETOWER_CHANNEL);
    return intToMapLocation(data);
  }

  public void sendNoisetowerBuilderId(int noisetowerBuilderId) throws GameActionException {
    rc.broadcast(NOISETOWER_BUILDER_CHANNEL, noisetowerBuilderId);
  }

  public int receiveNoisetowerBuilderId() throws GameActionException {
    return rc.readBroadcast(NOISETOWER_BUILDER_CHANNEL);
  }

  public void sendPastrDefendersNecessary(int defendersNecessary) throws GameActionException {
    rc.broadcast(PASTR_DEFENDERS_NECESSARY_CHANNEL, defendersNecessary);
  }

  public int receivePastrDefendersNecessary() throws GameActionException {
    return rc.readBroadcast(PASTR_DEFENDERS_NECESSARY_CHANNEL);
  }

  public void sendPastrDefendersClaimed(int defendersClaimed) throws GameActionException {
    rc.broadcast(PASTR_DEFENDERS_CLAIMED_CHANNEL, defendersClaimed);
  }

  public int receivePastrDefendersClaimed() throws GameActionException {
    return rc.readBroadcast(PASTR_DEFENDERS_CLAIMED_CHANNEL);
  }

  public void sendPastrDefenderId(int index, int id) throws GameActionException {
    assert(index < GameConstants.MAX_ROBOTS);
    rc.broadcast(PASTR_DEFENDERS_OFFSET + index, id);
  }

  public int receivePastrDefenderId(int index) throws GameActionException {
    return rc.readBroadcast(PASTR_DEFENDERS_OFFSET + index);
  }

  // Internal conversion methods.

  private static final int MAP_LOCATION_X_MULTIPLIER = 1000;

  private static int mapLocationToInt(MapLocation ml) {
    return ml.x * MAP_LOCATION_X_MULTIPLIER + ml.y;
  }

  private static MapLocation intToMapLocation(int data) {
    return new MapLocation(data / MAP_LOCATION_X_MULTIPLIER, data % MAP_LOCATION_X_MULTIPLIER);
  }

  private static final int YES = 1;
  private static final int NO = 0;

  private static int booleanToInt(boolean b) {
    return b ? YES : NO;
  }

  private static boolean intToBoolean(int data) {
    return (data == YES);
  }
}
