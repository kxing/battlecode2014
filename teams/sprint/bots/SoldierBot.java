package sprint.bots;

import battlecode.common.*;
import sprint.nav.*;
import sprint.sensing.*;

/**
 * Code for soldier-specific behavior.
 *
 * @author kxing
 */
public class SoldierBot extends Bot {
  private BugNav bugNav;

  private MapLocation MY_LOCATION;

  private static final double SELF_ATTACK_BONUS = 15.0;
  private static final double NEAR_ENEMY_PASTR_BONUS = 5.0;
  private static final double ENEMY_SOLDIER_RANGE_PENALTY = -10.0;
  private static final double OCCUPIED_ENEMY_SOLDIER_RANGE_BONUS = 7.0;
  private static final double ENEMY_TOWER_RANGE_PENALTY = -50.0;
  private static final double ENEMY_TOWER_SPLASH_PENALTY = -25.0;
  private static final double ILLEGAL_MOVE = -1.0e9;

  private MapLocation idealPastrLocation;
  private boolean isFarmer;
  
  private MapLocation currentGoal;
  private DirectionEvaluations directionEvaluations;
  private SensorInfo sensorInfo;

  private static final double ACTION_DELAY_UNSAFE = 0.5;
  private static final double ACTION_DEALY_REDUCTION_BONUS = 0.5;

  private static final double ENEMY_COWS_THRESHOLD = 50.0;

  public static final int FARMING_QUOTA = 30;

  public SoldierBot(RobotController rc) {
    super(rc);
    this.bugNav = new BugNav(rc);

    this.currentGoal = rc.senseEnemyHQLocation();
    this.directionEvaluations = new DirectionEvaluations();
    this.bugNav.setGoal(currentGoal);
    this.sensorInfo = new SensorInfo(rc);
    this.isFarmer = false;
  }

  public void performRoundActions() throws GameActionException {
    MY_LOCATION = rc.getLocation();

    if (!rc.isActive()) {
      return;
    }
    sensorInfo.clear();
    directionEvaluations.clear();

    MapLocation newGoal = getGoal();
    if (!currentGoal.equals(newGoal)) {
      currentGoal = newGoal;
      bugNav.setGoal(newGoal);
    }

    // Stop if we have made an attacking move.
    if (attackingMove()) {
      bugNav.setGoal(currentGoal);
      return;
    }

    if (communication.receiveFarmSignal()) {
      idealPastrLocation = communication.receivePastrLocation();
      if (communication.recieveFarmerId() == 0 && rc.canSenseSquare(ENEMY_TEAM_HEADQUARTERS)) {
        communication.sendFarmerId(rc.getRobot().getID());
        isFarmer = true;
      }
      if (isFarmer && MY_LOCATION.equals(idealPastrLocation)) {
        rc.construct(RobotType.PASTR);
        return;
      }
    }

    bugNav.moveTowardsGoal(directionEvaluations);
  }

  // Returns we should stop.
  private boolean attackingMove() throws GameActionException {
    sensorInfo.senseEnemyRobots(true);

    if (sensorInfo.getEnemyRobots().length == 0) {
      return false;
    }
    sensorInfo.senseMyRobots(false);

    for (Direction d : DirectionEvaluations.DIRECTIONS) {
      MapLocation candidateSquare = MY_LOCATION.add(d);

      if (d != Direction.NONE && !rc.canMove(d)) {
        directionEvaluations.addToDirection(ILLEGAL_MOVE, d);
        continue;
      }
      
      for (RobotInfo ri : sensorInfo.getEnemyInfos()) {
        switch (ri.type) {
          case HQ:
            if (ri.location.distanceSquaredTo(candidateSquare) <= RobotType.HQ.attackRadiusMaxSquared) {
              directionEvaluations.addToDirection(ENEMY_TOWER_RANGE_PENALTY, d);
            } else {
              Direction splashDirection = ri.location.directionTo(candidateSquare);
              if (ri.location.add(splashDirection).distanceSquaredTo(candidateSquare) <= RobotType.HQ.attackRadiusMaxSquared) {
                directionEvaluations.addToDirection(ENEMY_TOWER_SPLASH_PENALTY, d);
              }
            }
            break;
          case SOLDIER:
            if (MY_LOCATION.distanceSquaredTo(ri.location) > RobotType.SOLDIER.attackRadiusMaxSquared &&
                    rc.senseNearbyGameObjects(Robot.class, ri.location, RobotType.SOLDIER.attackRadiusMaxSquared, MY_TEAM).length > 0) {
              directionEvaluations.addToDirection(OCCUPIED_ENEMY_SOLDIER_RANGE_BONUS / (candidateSquare.distanceSquaredTo(ri.location) + 1), d);
            } else if (sensorInfo.getEnemyRobots().length * 2 < sensorInfo.getMyRobots().length) {
              // Don't care - the opponent is greatly outnumbered.
            } else if (ri.location.distanceSquaredTo(candidateSquare) <= RobotType.SOLDIER.attackRadiusMaxSquared) {
              directionEvaluations.addToDirection(ENEMY_SOLDIER_RANGE_PENALTY, d);
            }
            break;
          case PASTR:
          case NOISETOWER:
            if (unhinderedPathTo(candidateSquare, ri.location)) {
              directionEvaluations.addToDirection(
                      NEAR_ENEMY_PASTR_BONUS / (candidateSquare.distanceSquaredTo(ri.location) + 1), d);
            }
            break;
          default:
            // Don't care.
            break;
        }
      }
    }

    // Consider attacking.
    for (RobotInfo ri : sensorInfo.getEnemyInfos()) {
      if (ri.type != RobotType.HQ && ri.location.distanceSquaredTo(MY_LOCATION) <= ATTACK_RADIUS_SQUARED) {
        directionEvaluations.addToDirection(SELF_ATTACK_BONUS, Direction.NONE);
        break;
      }
    }

    if (rc.getActionDelay() >= ACTION_DELAY_UNSAFE) {
      directionEvaluations.addToDirection(ACTION_DEALY_REDUCTION_BONUS, Direction.NONE);
    }

    Direction bestMovement = directionEvaluations.getBestDirection();

    if (directionEvaluations.isMoveForced()) {
      if (bestMovement != DirectionEvaluations.ATTACK_SENTINEL) {
        rc.move(bestMovement);
      } else {
        attack();
      }
      return true;
    }
    return false;
  }

  private MapLocation getGoal() throws GameActionException {
    if (isFarmer) {
      if (rc.sensePastrLocations(MY_TEAM).length > 0 && MY_LOCATION.distanceSquaredTo(idealPastrLocation) <= 8) {
        isFarmer = false;
        communication.sendFarmerId(0);
      } else {
        return idealPastrLocation;
      }
    }
    
    MapLocation[] enemyPastrs = rc.sensePastrLocations(ENEMY_TEAM);

    MapLocation bestTarget = ENEMY_TEAM_HEADQUARTERS;
    int bestDistance = 0;
    for (int i = 0; i < enemyPastrs.length; i++) {
      if (bestDistance < enemyPastrs[i].distanceSquaredTo(ENEMY_TEAM_HEADQUARTERS)) {
        bestDistance = enemyPastrs[i].distanceSquaredTo(ENEMY_TEAM_HEADQUARTERS);
        bestTarget = enemyPastrs[i];
      }
    }

    return bestTarget;
  }

  private boolean unhinderedPathTo(MapLocation start, MapLocation target) {
    MapLocation current = start;
    while (!current.equals(target)) {
      Direction d = current.directionTo(target);
      current = current.add(d);
      switch (rc.senseTerrainTile(current)) {
        case VOID:
        case OFF_MAP:
          return false;
        default:
          break;
      }
    }
    return true;
  }

  private void attack() throws GameActionException {
    MapLocation bestTarget = null;
    RobotType bestTargetType = null;
    double bestTargetHp = Double.MAX_VALUE;
    
    for (RobotInfo ri : sensorInfo.getEnemyInfos()) {
      MapLocation potentialTarget = ri.location;
      if (MY_LOCATION.distanceSquaredTo(potentialTarget) > ATTACK_RADIUS_SQUARED) {
        continue;
      }

      boolean isBetterTarget = false;
      // soldier > pastr > noisetower.
      switch (ri.type) {
        case SOLDIER:
          // Prioritize soldiers, especially ones with the least amount of health.
          if (bestTargetType != RobotType.SOLDIER || ri.health < bestTargetHp) {
            isBetterTarget = true;
          }
          break;
        case PASTR:
          // Prioritize pastrs after soldiers.
          if (bestTargetType != RobotType.SOLDIER &&
                  (bestTargetType != RobotType.PASTR || ri.health < bestTargetHp)) {
            isBetterTarget = true;
          }
          break;
        case NOISETOWER:
          if (bestTargetType != RobotType.SOLDIER && bestTargetType != RobotType.PASTR &&
                  (bestTargetType != RobotType.NOISETOWER || ri.health < bestTargetHp)) {
            isBetterTarget = true;
          }
          break;
        default:
          break;
      }

      if (isBetterTarget) {
        bestTarget = potentialTarget;
        bestTargetType = ri.type;
        bestTargetHp = ri.health;
      }
    }

    if (bestTarget == null) {
      // Attack a square with cows that is under the enemy pastr.
      if (!currentGoal.equals(ENEMY_TEAM_HEADQUARTERS) && rc.canSenseSquare(currentGoal)) {
        Direction d = currentGoal.directionTo(MY_LOCATION);
        // TODO(kxing): Check if this check is necessary.
        if (d != Direction.NONE && d != Direction.OMNI) {
          MapLocation[] potentialSquares = new MapLocation[] {
            currentGoal.add(d),
            currentGoal.add(d).add(d),
            currentGoal.add(d).add(d.rotateLeft()),
            currentGoal.add(d).add(d.rotateRight()),
          };

          double mostCows = ENEMY_COWS_THRESHOLD;
          for (MapLocation ml : potentialSquares) {
            if (!rc.canAttackSquare(ml) || rc.senseObjectAtLocation(ml) != null) {
              continue;
            }
            double cows = rc.senseCowsAtLocation(ml);
            if (mostCows < cows) {
              bestTarget = ml;
              mostCows = cows;
            }
          }
        }
      }
    }

    if (bestTarget != null) {
      rc.attackSquare(bestTarget);
    }
  }
}
