package sprint.bots;

import battlecode.common.*;
import sprint.communication.*;

/**
 * Generic behavior for all bots.
 *
 * @author kxing
 */
public abstract class Bot {
  
  protected RobotController rc;
  protected Communication communication;

  protected final Team MY_TEAM;
  protected final Team ENEMY_TEAM;
  protected final MapLocation MY_TEAM_HEADQUARTERS;
  protected final MapLocation ENEMY_TEAM_HEADQUARTERS;
  
  protected final int SENSOR_RADIUS_SQUARED;
  protected final int ATTACK_RADIUS_SQUARED;

  public Bot(RobotController rc) {
    this.rc = rc;
    this.communication = new Communication(rc);
    
    this.MY_TEAM = rc.getTeam();
    this.ENEMY_TEAM = rc.getTeam().opponent();
    this.MY_TEAM_HEADQUARTERS = rc.senseHQLocation();
    this.ENEMY_TEAM_HEADQUARTERS = rc.senseEnemyHQLocation();
    
    this.SENSOR_RADIUS_SQUARED = rc.getType().sensorRadiusSquared;
    this.ATTACK_RADIUS_SQUARED = rc.getType().attackRadiusMaxSquared;
  }
  
  public void run() {
		while(true) {
      try {
        performRoundActions();
      } catch (Exception e) {
        e.printStackTrace();
      }

			rc.yield();
		}
  }

  public abstract void performRoundActions() throws GameActionException;
}
