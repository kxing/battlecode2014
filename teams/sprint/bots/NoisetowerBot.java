package sprint.bots;

import battlecode.common.*;

/**
 *
 * @author kxing
 */
public class NoisetowerBot extends Bot {

  public NoisetowerBot(RobotController rc) {
    super(rc);
  }

  public void performRoundActions() throws GameActionException {
    // Blank for now.
  }

}
