package sprint.bots;

import battlecode.common.*;

/**
 * Code for pastr-specific behavior.
 *
 * @author kxing
 */
public class PastrBot extends Bot {

  public PastrBot(RobotController rc) {
    super(rc);
  }

  public void performRoundActions() throws GameActionException {
    // Do nothing (so far).
  }
}
