package sprint.bots;

import battlecode.common.*;
import sprint.utils.Utils;

/**
 * Code for headquarters-specific behavior.
 *
 * @author kxing
 */
public class HeadquartersBot extends Bot {

  private final MapLocation MY_LOCATION;

  private final MapLocation spawnSquare;
  private final MapLocation idealPastrLocation;

  public HeadquartersBot(RobotController rc) {
    super(rc);
    this.MY_LOCATION = rc.getLocation();
    this.spawnSquare = computeSpawnSquare();
    this.idealPastrLocation = computePastrLocation();
  }

  public void performRoundActions() throws GameActionException {
    if (!rc.isActive()) {
      return;
    }

    if (rc.senseNearbyGameObjects(Robot.class, ENEMY_TEAM_HEADQUARTERS, 50, MY_TEAM).length >= SoldierBot.FARMING_QUOTA) {
      communication.sendFarmSignal(true, idealPastrLocation);
    }

    int farmerId = communication.recieveFarmerId();
    if (farmerId != 0) {
      Robot[] myRobots = rc.senseNearbyGameObjects(Robot.class,
              GameConstants.MAP_MAX_WIDTH * GameConstants.MAP_MAX_WIDTH + GameConstants.MAP_MAX_HEIGHT * GameConstants.MAP_MAX_HEIGHT, MY_TEAM);
      boolean foundFarmer = false;
      for (Robot r : myRobots) {
        if (r.getID() == farmerId) {
          foundFarmer = true;
          break;
        }
      }

      if (!foundFarmer) {
        communication.sendFarmerId(0);
      }
    }

    // TODO(kxing): Make this smarter.
    Robot[] enemyRobots = rc.senseNearbyGameObjects(Robot.class, SENSOR_RADIUS_SQUARED, ENEMY_TEAM);
    RobotInfo[] enemyInfos = new RobotInfo[enemyRobots.length];

    for (int i = 0; i < enemyRobots.length; i++) {
      enemyInfos[i] = rc.senseRobotInfo(enemyRobots[i]);
    }

    for (int i = 0; i < enemyRobots.length; i++) {
      if (enemyInfos[i].location.distanceSquaredTo(MY_LOCATION) <= ATTACK_RADIUS_SQUARED) {
        rc.attackSquare(enemyInfos[i].location);
        return;
      }
    }

    for (int i = 0; i < enemyRobots.length; i++) {
      MapLocation enemyLocation = enemyInfos[i].location;
      Direction d = enemyLocation.directionTo(MY_LOCATION);
      if (enemyLocation.add(d).distanceSquaredTo(MY_LOCATION) <= ATTACK_RADIUS_SQUARED) {
        rc.attackSquare(enemyLocation.add(d));
        return;
      }
    }

    // TODO(kxing): Spawn smarter if we are losing.
    // Check if a robot is spawnable and spawn one if it is
    if (rc.senseRobotCount() < GameConstants.MAX_ROBOTS) {
      if (rc.senseObjectAtLocation(spawnSquare) == null) {
        rc.spawn(MY_TEAM_HEADQUARTERS.directionTo(spawnSquare));
      }
    }
  }

  private MapLocation computeSpawnSquare() {
    Direction toEnemy = rc.getLocation().directionTo(ENEMY_TEAM_HEADQUARTERS);
    Direction[] directions = new Direction[] {
      toEnemy,
      toEnemy.rotateRight(),
      toEnemy.rotateLeft(),
      toEnemy.rotateRight().rotateRight(),
      toEnemy.rotateLeft().rotateLeft(),
      toEnemy.opposite().rotateLeft(),
      toEnemy.opposite().rotateRight(),
      toEnemy.opposite(),
    };

    for (Direction d : directions) {
      if (Utils.isTraversable(rc.senseTerrainTile(MY_TEAM_HEADQUARTERS.add(d)))) {
        return MY_TEAM_HEADQUARTERS.add(d);
      }
    }
    return null;
  }

  // Must be called after computing the spawn square.
  private MapLocation computePastrLocation() {
    Direction d = MY_TEAM_HEADQUARTERS.directionTo(ENEMY_TEAM_HEADQUARTERS).opposite();

    MapLocation[] testSquares = new MapLocation[] {
      MY_TEAM_HEADQUARTERS.add(d).add(d),
      MY_TEAM_HEADQUARTERS.add(d).add(d.rotateRight()),
      MY_TEAM_HEADQUARTERS.add(d).add(d.rotateLeft()),
      MY_TEAM_HEADQUARTERS.add(d.rotateRight()).add(d.rotateRight()),
      MY_TEAM_HEADQUARTERS.add(d.rotateLeft()).add(d.rotateLeft()),
      MY_TEAM_HEADQUARTERS.add(d),
      MY_TEAM_HEADQUARTERS.add(d.rotateRight()),
      MY_TEAM_HEADQUARTERS.add(d.rotateLeft()),
    };
    for (MapLocation testSquare : testSquares) {
      if (Utils.isTraversable(rc.senseTerrainTile(testSquare))) {
        return testSquare;
      }
    }

    for (int x = MY_TEAM_HEADQUARTERS.x - 2; x <= MY_TEAM_HEADQUARTERS.x + 2; x++) {
      for (int y = MY_TEAM_HEADQUARTERS.y - 2; y <= MY_TEAM_HEADQUARTERS.y + 2; y++) {
        MapLocation testLocation = new MapLocation(x, y);
        if (Utils.isTraversable(rc.senseTerrainTile(testLocation)) &&
                !testLocation.equals(spawnSquare) &&
                !testLocation.equals(MY_TEAM_HEADQUARTERS)) {
          return testLocation;
        }
      }
    }
    return null;
  }
}
