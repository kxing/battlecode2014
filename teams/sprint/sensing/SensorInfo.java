package sprint.sensing;

import battlecode.common.*;

/**
 * A convenience structure to hold sensing information.
 * 
 * @author kxing
 */
public class SensorInfo {

  private final RobotController rc;
  private final int SENSOR_RANGE;
  private final Team MY_TEAM;
  private final Team ENEMY_TEAM;

  private Robot[] enemyRobots;
  private RobotInfo[] enemyInfos;
  private Robot[] myRobots;
  private RobotInfo[] myInfos;


  public SensorInfo(RobotController rc) {
    this.rc = rc;
    this.SENSOR_RANGE = rc.getType().sensorRadiusSquared;
    this.MY_TEAM = rc.getTeam();
    this.ENEMY_TEAM = MY_TEAM.opponent();
  }

  public void clear() {
    enemyRobots = null;
    enemyInfos = null;
    myRobots = null;
    myInfos = null;
  }

  public void senseEnemyRobots(boolean withInfos) throws GameActionException {
    enemyRobots = rc.senseNearbyGameObjects(Robot.class, SENSOR_RANGE, ENEMY_TEAM);

    if (withInfos) {
      enemyInfos = new RobotInfo[enemyRobots.length];
      for (int i = 0; i < enemyRobots.length; i++) {
        enemyInfos[i] = rc.senseRobotInfo(enemyRobots[i]);
      }
    }
  }

  public Robot[] getEnemyRobots() {
    return enemyRobots;
  }

  public RobotInfo[] getEnemyInfos() {
    return enemyInfos;
  }

  public void senseMyRobots(boolean withInfos) throws GameActionException {
    myRobots = rc.senseNearbyGameObjects(Robot.class, SENSOR_RANGE, MY_TEAM);

    if (withInfos) {
      myInfos = new RobotInfo[myRobots.length];
      for (int i = 0; i < myRobots.length; i++) {
        myInfos[i] = rc.senseRobotInfo(myRobots[i]);
      }
    }
  }

  public Robot[] getMyRobots() {
    return myRobots;
  }

  public RobotInfo[] getMyInfos() {
    return myInfos;
  }

}
