package sprint.nav;

import battlecode.common.*;
import java.util.*;
import sprint.utils.Utils;

/**
 * Bug navigation.
 *
 * @author kxing
 */
public class BugNav {
  private RobotController rc;

  private MapLocation goal;

  private boolean isBugging;
  private Direction lastDirection;
  private boolean nextBugCounterclockwise;

  private MapLocation wallPoint;

  private HashSet<MapLocation> visited;

  public BugNav(RobotController rc) {
    this.rc = rc;
    this.isBugging = false;
    this.nextBugCounterclockwise = true;
    this.visited = new HashSet<MapLocation>();

    // TODO(kxing): Make this less arbitrary.
    this.lastDirection = Direction.NORTH;
  }

  public void setGoal(MapLocation goal) {
    // TODO(kxing): Make this logic more watertight.
    if (!goal.equals(this.goal)) {
      nextBugCounterclockwise = true;
    }
    this.goal = goal;
    isBugging = false;
  }

  private boolean tryMove(Direction d, DirectionEvaluations directionEvaluation) throws GameActionException {
    if (!rc.canMove(d)) {
      return false;
    }
    if (directionEvaluation.isDangerous(d)) {
      return false;
    }
    if (rc.getLocation().add(d).distanceSquaredTo(rc.senseEnemyHQLocation()) <= RobotType.HQ.attackRadiusMaxSquared) {
      return false;
    }
    rc.move(d);
    lastDirection = d;
    return true;
  }

  public boolean moveTowardsGoal(DirectionEvaluations directionEvaluations) throws GameActionException {
    if (!rc.isActive()) {
      return false;
    }
    if (rc.getLocation().equals(goal)) {
      return true;
    }
    MapLocation current = rc.getLocation();
    Direction goalDirection = current.directionTo(goal);

    if (!isBugging) {
      if (tryMove(goalDirection, directionEvaluations)) {
        return true;
      }
      if (tryMove(goalDirection.rotateLeft(), directionEvaluations)) {
        return true;
      }
      if (tryMove(goalDirection.rotateRight(), directionEvaluations)) {
        return true;
      }
      isBugging = true;
      wallPoint = current.add(goalDirection);
    }

    // Only do counterclockwise bugging.
    if (!visited.contains(current)
            && !Utils.areDirectionsWithin45(goalDirection, lastDirection.opposite())
            && tryMove(goalDirection, directionEvaluations)) {
      isBugging = false;
      visited.add(current);
      return true;
    }

    // TODO(kxing): Less duplicate code.
    Direction directionToWall = current.directionTo(wallPoint);
    if (nextBugCounterclockwise) {
      Direction testDirection = directionToWall.rotateRight();
      do {
        if (tryMove(testDirection, directionEvaluations)) {
          if (rc.senseTerrainTile(wallPoint) == TerrainTile.OFF_MAP) {
            if (Utils.areDirectionsWithin45(goalDirection, lastDirection.opposite())) {
              nextBugCounterclockwise = !nextBugCounterclockwise;
            }
          }
          return true;
        }
        wallPoint = current.add(testDirection);
        testDirection = testDirection.rotateRight();
      } while (testDirection != directionToWall);
    } else {
      Direction testDirection = directionToWall.rotateLeft();
      do {
        if (tryMove(testDirection, directionEvaluations)) {
          if (rc.senseTerrainTile(wallPoint) == TerrainTile.OFF_MAP) {
            if (Utils.areDirectionsWithin45(goalDirection, lastDirection.opposite())) {
              nextBugCounterclockwise = !nextBugCounterclockwise;
            }
          }
          return true;
        }
        wallPoint = current.add(testDirection);
        testDirection = testDirection.rotateLeft();
      } while (testDirection != directionToWall);
    }


    return false;
  }
}
