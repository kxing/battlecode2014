package sprint.nav;

import sprint.utils.*;

import battlecode.common.*;
import java.util.*;

/**
 * A map that keeps track of evaluations of moving in each direction.
 * Direction.NONE corresponds to attacking.
 *
 * @author kxing
 */
public class DirectionEvaluations {

  private final double[] evaluations;

  private static final double EPSILON = 0.001;

  private static final int NORTH_INDEX = 0;
  private static final int NORTH_EAST_INDEX = 1;
  private static final int EAST_INDEX = 2;
  private static final int SOUTH_EAST_INDEX = 3;
  private static final int SOUTH_INDEX = 4;
  private static final int SOUTH_WEST_INDEX = 5;
  private static final int WEST_INDEX = 6;
  private static final int NORTH_WEST_INDEX = 7;
  private static final int NONE_INDEX = 8;

  public static final Direction[] DIRECTIONS = new Direction[] {
    Direction.NORTH,
    Direction.NORTH_EAST,
    Direction.EAST,
    Direction.SOUTH_EAST,
    Direction.SOUTH,
    Direction.SOUTH_WEST,
    Direction.WEST,
    Direction.NORTH_WEST,
    Direction.NONE
  };

  public static final Direction ATTACK_SENTINEL = Direction.NONE;

  public DirectionEvaluations() {
    this.evaluations = new double[Utils.ATTACK_MICRO_DIRECTIONS.length];
  }

  public void clear() {
    Arrays.fill(this.evaluations, 0.0);
  }

  public void addToDirection(double value, Direction d) {
    evaluations[directionToIndex(d)] += value;
  }

  public boolean isMoveForced() {
    boolean allNegative = true;
    
    for (double evaluation : evaluations) {
      if (evaluation > EPSILON) {
        return true;
      } else if (evaluation > -EPSILON) {
        allNegative = false;
      }
    }

    return allNegative;
  }

  public Direction getBestDirection() {
    double bestValue = Double.NEGATIVE_INFINITY;
    int bestIndex = 0;

    for (int i = 0; i < evaluations.length; i++) {
      if (bestValue < evaluations[i]) {
        bestValue = evaluations[i];
        bestIndex = i;
      }
    }

    return DIRECTIONS[bestIndex];
  }

  public boolean isDangerous(Direction d) {
    return (evaluations[directionToIndex(d)] < -EPSILON);
  }

  private int directionToIndex(Direction d) {
    switch (d) {
      case NORTH:
        return NORTH_INDEX;
      case NORTH_EAST:
        return NORTH_EAST_INDEX;
      case EAST:
        return EAST_INDEX;
      case SOUTH_EAST:
        return SOUTH_EAST_INDEX;
      case SOUTH:
        return SOUTH_INDEX;
      case SOUTH_WEST:
        return SOUTH_WEST_INDEX;
      case WEST:
        return WEST_INDEX;
      case NORTH_WEST:
        return NORTH_WEST_INDEX;
      case NONE:
        return NONE_INDEX;
      default:
        System.err.println(String.format("Invalid direction: %s", d.toString()));
        return 0;
    }
  }
}
