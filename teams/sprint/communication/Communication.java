package sprint.communication;

import battlecode.common.*;

/**
 *
 * @author kxing
 */
public class Communication {

  private final RobotController rc;

  private static final int FARM_CHANNEL = 0;
  private static final int FARM_PASTR_CHANNEL = 1;
  private static final int FARMER_ID_CHANNEL = 2;

  public Communication(RobotController rc) {
    this.rc = rc;
  }

  public void sendFarmSignal(boolean shouldFarm, MapLocation location) throws GameActionException {
    rc.broadcast(FARM_CHANNEL, shouldFarm ? 1 : 0);
    rc.broadcast(FARM_PASTR_CHANNEL, location.x * 1000 + location.y);
  }

  public boolean receiveFarmSignal() throws GameActionException {
    return rc.readBroadcast(FARM_CHANNEL) == 1;
  }

  public MapLocation receivePastrLocation() throws GameActionException {
    int data = rc.readBroadcast(FARM_PASTR_CHANNEL);
    return new MapLocation(data / 1000, data % 1000);
  }

  public void sendFarmerId(int farmerId) throws GameActionException {
    rc.broadcast(FARMER_ID_CHANNEL, farmerId);
  }

  public int recieveFarmerId() throws GameActionException {
    return rc.readBroadcast(FARMER_ID_CHANNEL);
  }
}
