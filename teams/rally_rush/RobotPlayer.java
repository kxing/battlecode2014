package rally_rush;

import battlecode.common.*;

/**
 * Main entry point.
 * 
 * @author kxing
 */
public class RobotPlayer {
	
	public static void run(RobotController rc) {
    while (true) {
      try {
        switch(rc.getType()) {
          case SOLDIER:
            SoldierCode.runSoldier(rc);
            break;
          case HQ:
            HeadquartersCode.runHq(rc);
            break;
          case PASTR:
            break;
          case NOISETOWER:
            break;
          default:
            System.err.println(String.format("Invalid type detected: %s",
                    rc.getType().toString()));
        }
      } catch (Exception e) {
        e.printStackTrace();
      }
    }
	}

  public static class SoldierCode {
    private static final int SWARM_SIZE = 3;
    private static final int SAFE_DISTANCE = 26;
    
    private static boolean shouldGo = false;
    
    public static void runSoldier(RobotController rc) throws GameActionException {
      if (!shouldGo) {
        if (rc.senseNearbyGameObjects(Robot.class, RobotType.SOLDIER.sensorRadiusSquared, rc.getTeam()).length >= SWARM_SIZE) {
          shouldGo = true;
        }
      }
      
      if (rc.isActive()) {
        boolean doneAction = false;
        
        Robot[] enemyRobots = rc.senseNearbyGameObjects(Robot.class, RobotType.SOLDIER.attackRadiusMaxSquared, rc.getTeam().opponent());
        RobotInfo[] enemyInfos = new RobotInfo[enemyRobots.length];
        for (int i = 0; i < enemyInfos.length; i++) {
          enemyInfos[i] = rc.senseRobotInfo(enemyRobots[i]);
          if (enemyInfos[i].type != RobotType.HQ) {
            rc.attackSquare(enemyInfos[i].location);
            doneAction = true;
            break;
          }
        }

        if (!doneAction && shouldGo) {
          MapLocation target = getTarget(rc);
          Direction directionToTarget = rc.getLocation().directionTo(target);
          Direction[] directions = new Direction[] {
            directionToTarget,
            directionToTarget.rotateLeft(),
            directionToTarget.rotateRight(),
            directionToTarget.rotateLeft().rotateLeft(),
            directionToTarget.rotateRight().rotateRight(),
          };
          for (Direction d : directions) {
            if (rc.canMove(d) &&
                rc.getLocation().add(d).distanceSquaredTo(rc.senseEnemyHQLocation()) >= SAFE_DISTANCE) {
              rc.move(d);
              doneAction = true;
              break;
            }
          }
        }
        
      }

      rc.yield();
    }

    private static MapLocation getTarget(RobotController rc) throws GameActionException {
      MapLocation[] enemyPastrs = rc.sensePastrLocations(rc.getTeam().opponent());
      MapLocation enemyHeadquarters = rc.senseEnemyHQLocation();
      
      MapLocation bestTarget = enemyHeadquarters;

      for (MapLocation ml : enemyPastrs) {
        if (bestTarget.distanceSquaredTo(enemyHeadquarters) < ml.distanceSquaredTo(enemyHeadquarters)) {
          bestTarget = ml;
        }
      }

      return bestTarget;
    }
  }

  public static class HeadquartersCode {
    public static void runHq(RobotController rc) throws GameActionException {
      // Spawn if we can.
      if (rc.isActive()) {
        if (!attack(rc)) {
          // Spawn if we don't attack.
          spawn(rc);
        }
      }

      rc.yield();
    }

    // Returns whether we actually attacked.
    private static boolean attack(RobotController rc) throws GameActionException {
      Robot[] enemyRobots = rc.senseNearbyGameObjects(Robot.class, rc.getType().sensorRadiusSquared, rc.getTeam().opponent());
      RobotInfo[] enemyInfos = new RobotInfo[enemyRobots.length];

      for (int i = 0; i < enemyInfos.length; i++) {
        enemyInfos[i] = rc.senseRobotInfo(enemyRobots[i]);
      }

      MapLocation MY_LOCATION = rc.getLocation();
      int ATTACK_RADIUS_SQUARED = rc.getType().attackRadiusMaxSquared;

      for (RobotInfo ri : enemyInfos) {
        if (ri.location.distanceSquaredTo(MY_LOCATION) <= ATTACK_RADIUS_SQUARED) {
          rc.attackSquare(ri.location);
          return true;
        }
      }

      for (RobotInfo ri : enemyInfos) {
        MapLocation enemyLocation = ri.location;
        Direction d = enemyLocation.directionTo(MY_LOCATION);
        if (enemyLocation.add(d).distanceSquaredTo(MY_LOCATION) <= ATTACK_RADIUS_SQUARED) {
          rc.attackSquare(enemyLocation.add(d));
          return true;
        }
      }

      return false;
    }

    public static boolean spawn(RobotController rc) throws GameActionException {
      if (rc.senseRobotCount() < GameConstants.MAX_ROBOTS) {
        Direction startSpawnDirection = rc.getLocation().directionTo(rc.senseEnemyHQLocation());

        Direction[] spawnDirections = new Direction[] {
          startSpawnDirection,
          startSpawnDirection.rotateLeft(),
          startSpawnDirection.rotateRight(),
          startSpawnDirection.rotateLeft().rotateLeft(),
          startSpawnDirection.rotateRight().rotateRight(),
          startSpawnDirection.opposite().rotateRight(),
          startSpawnDirection.opposite().rotateLeft(),
          startSpawnDirection.opposite(),
        };

        for (Direction spawnDirection : spawnDirections) {
          MapLocation spawnSquare = rc.getLocation().add(spawnDirection);
          if (rc.senseObjectAtLocation(spawnSquare) == null) {
            TerrainTile tile = rc.senseTerrainTile(spawnSquare);
            if (tile != TerrainTile.OFF_MAP && tile != TerrainTile.VOID) {
              rc.spawn(spawnDirection);
              return true;
            }
          }
        }
      }
      return false;
    }
  }

}
