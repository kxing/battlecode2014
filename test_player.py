#!/usr/bin/python

import os
import re
import shutil
import subprocess

MAIN_TEAM = 'team213'

maps = [ \
  'almsman', \
  'backdoor', \
  'bakedpotato', \
  'blocky', \
  'cadmic', \
  'castles', \
  'corners', \
  'desolation', \
  'divide', \
  'donut', \
  'fenced', \
  'filling', \
  'flagsoftheworld', \
  'flytrap', \
  'friendly', \
  'fuzzy', \
  'gilgamesh', \
  'highschool', \
  'highway', \
  'house', \
  'hydratropic', \
  'hyperfine', \
  'intermeningeal', \
  'itsatrap', \
  'librarious', \
  'magnetism', \
  'meander', \
  'moba', \
  'moo', \
  'neighbors', \
  'oasis', \
  'onramp', \
  'overcast', \
  'pipes', \
  'race', \
  'reticle', \
  'rushlane', \
  's1', \
  'siege', \
  'smiles', \
  'spots', \
  'spyglass', \
  'steamedbuns', \
  'stitch', \
  'sweetspot', \
  'temple', \
  'terra', \
  'traffic', \
  'troll', \
  'unself', \
  'valve', \
  'ventilation', \
]

simple_players = [ \
  'nullplayer', \
  'examplefuncsplayer', \
]

CONFIG_FILE = 'bc.conf'
CONFIG_FILE_TESTING = 'bc.conf.testing'
CONFIG_FILE_BACKUP = 'bc.conf.backup'

TEST_DIRECTORY = 'test/'
TEST_OUTPUT_TEMP_FILE = 'test/tmp.txt'

A_WINS = 0
B_WINS = 1

def set_up():
  shutil.copy(CONFIG_FILE, CONFIG_FILE_BACKUP)
  if not os.path.exists(TEST_DIRECTORY):
    os.mkdir(TEST_DIRECTORY)

def tear_down():
  shutil.move(CONFIG_FILE_BACKUP, CONFIG_FILE)

def run_game(team_a, team_b, map):
  match_file = "test/%s-%s-%s.rms" % (team_a, team_b, map)

  os.system("cat %s | "
            "sed 's:XXXmapsXXX:%s:g' | "
            "sed 's:XXXteam_aXXX:%s:g' | "
            "sed 's:XXXteam_bXXX:%s:g' | "
            "sed 's:XXXmatch_fileXXX:%s:g' >%s" %
            (CONFIG_FILE_TESTING, map, team_a, team_b, match_file, CONFIG_FILE))

  os.system("ant file >%s" % TEST_OUTPUT_TEMP_FILE)
  output_data = open(TEST_OUTPUT_TEMP_FILE, 'r').read()

  team_a_win_text = "%s \(A\) wins" % team_a
  team_b_win_text = "%s \(B\) wins" % team_b

  if len(re.findall(team_a_win_text, output_data)) > 0:
    return A_WINS
  elif len(re.findall(team_b_win_text, output_data)) > 0:
    return B_WINS
  else:
    print "Error on map %s between %s and %s." % (map, team_a, team_b)
    os.exit(1)

if __name__ == "__main__":
  set_up()

  for player in simple_players:
    print "Testing against player: %s" % player
    for map in maps:
      print "\tTesting on map: %s" % map

      print "\t\tTesting as team A:",
      # Test as team A.
      if run_game(MAIN_TEAM, player, map) == B_WINS:
        print "Lost as team A against %s on %s" % (player, map)
      else:
        print "OK"

      print "\t\tTesting as team B:",
      # Test as team B.
      if run_game(player, MAIN_TEAM, map) == A_WINS:
        print "Lost as team B against %s on %s" % (player, map)
      else:
        print "OK"

  tear_down()
